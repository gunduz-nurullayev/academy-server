import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.nazarov.software.server.hibernate.tool.SearchModel;
import org.nazarov.software.server.hibernate.tool.enums.SearchAttribute;
import org.nazarov.software.server.hibernate.tool.enums.UserMode;
import org.nazarov.software.server.spring.api.model.SearchFilterModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.*;

/**
 * Created by Shahin on 11/5/2016.
 */
public class JsonValue {

    private PasswordEncoder encoder = new BCryptPasswordEncoder();

    @Test
    public void test(){
        System.out.println(encoder.encode("shahin"));
    }


//    @Test
//    public void Test() throws JsonProcessingException {
//        ObjectMapper mapper = new ObjectMapper();
//        SearchFilterModel filterModel = new SearchFilterModel();
//        Set<SearchModel> models = new HashSet<SearchModel>();
//        SearchModel model = new SearchModel();
//        model.setName("test");
//        model.setValue("test");
//        model.setAttribute(SearchAttribute.GREATER);
//        models.add(model);
//        filterModel.setSearchModels(models);
//        Set<String> strings = new HashSet<String>();
//        strings.add("ok");
//        filterModel.setOrders(strings);
//        filterModel.setMaxResult(6);
//        filterModel.setFirstResult(0);
//        System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(filterModel));
//    }

}
