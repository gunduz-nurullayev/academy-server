import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.nazarov.software.server.hibernate.entity.Activity;
import org.nazarov.software.server.hibernate.entity.User;
import org.nazarov.software.server.hibernate.service.ActivityService;
import org.nazarov.software.server.hibernate.service.UserService;
import org.nazarov.software.server.hibernate.tool.SearchModel;
import org.nazarov.software.server.hibernate.tool.enums.SearchAttribute;
import org.nazarov.software.server.hibernate.tool.enums.UserRole;
import org.nazarov.software.server.spring.configuration.HibernateConfiguration;
import org.nazarov.software.server.spring.configuration.MailConfiguration;
import org.nazarov.software.server.spring.configuration.MvcConfiguration;
import org.nazarov.software.server.spring.security.AuthorizationServerConfiguration;
import org.nazarov.software.server.spring.security.MethodSecurityConfig;
import org.nazarov.software.server.spring.security.OAuth2SecurityConfiguration;
import org.nazarov.software.server.spring.security.ResourceServerConfiguration;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Shahin on 3/17/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {HibernateConfiguration.class})
public class SpringTest implements ApplicationContextAware{

    private ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext=applicationContext;
    }

    @Test
    public void test(){
        UserService userService = applicationContext.getBean(UserService.class);

        ActivityService activityService = applicationContext.getBean(ActivityService.class);

        org.junit.Assert.assertNotNull(activityService.list());
    }
}
