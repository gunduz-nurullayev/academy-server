package org.nazarov.software.server.tool.support;

import org.nazarov.software.server.tool.details.ProjectDetails;
import org.shaheen.nazarov.tools.util.details.DeveloperDetails;
import org.shaheen.nazarov.tools.util.details.DeveloperMessageMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by Shahin on 10/17/2016.
 */

public class ProjectSupport {

    @Autowired
    private JavaMailSender javaMailSender;

    public String getStackTrace(final Throwable throwable) {
        final StringWriter sw = new StringWriter();
        final PrintWriter pw = new PrintWriter(sw, true);
        throwable.printStackTrace(pw);
        return sw.getBuffer().toString();
    }

    public void sendMailToDeveloper(Throwable throwable, DeveloperMessageMode messageMode) {
        String text = getStackTrace(throwable);
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        text = text + "\n\n " + ProjectDetails.PROJECT_NAME +

                " ( " + ProjectDetails.PROJECT_URL +
                " :=> "+ProjectDetails.PROJECT_AUTHOR+")";

        mailMessage.setTo(DeveloperDetails.EMAIL.getValue());
        mailMessage.setSubject(messageMode.name() + "<" + DeveloperDetails.FULLNAME.getValue() + ">");
        mailMessage.setText(text);
        javaMailSender.send(mailMessage);
    }

}
