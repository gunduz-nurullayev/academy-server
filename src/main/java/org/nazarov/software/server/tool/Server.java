package org.nazarov.software.server.tool;

/**
 * Created by Shahin on 11/7/2016.
 */
public interface Server {
    String API = "/api";
    String PUBLIC = "/public";
    String PUBLIC_RECOVER = "/recover";
    String PUBLIC_FORGOT = "/forgot";
    String PUBLIC_SUBSCRIBE = "/subscribe";
    String PUBLIC_UNSUBSCRIBE = "/unsubscribe";
    String PUBLIC_FEEDBACK = "/feedback";
    String PUBLIC_DOWNLOAD = "/img";
    String PUBLIC_COURSES = "/courses";
    String PUBLIC_TEACHERS = "/teachers";
    String USER = "/users";
    String GROUP = "/groups";
    String ACTIVITY = "/activities";
    String COURSE = "/courses";
    String STUDENT = "/students";
    String TEACHER = "/teachers";
    String LESSON = "/lessons";
    String PAYMENT = "/payments";
    String TABLE = "/tables";
    String USERDETAIL = "/user/details";
    String MAILTEMPLATE = "/mail/templates";
    String FEEDBACK = "/feedbacks";
    String PROPERTYMAP = "/properties";
    String SUBSCRIBER = "/subscribers";
    String BILLING = "/billings";
    String IMAGE = "/images";
}
