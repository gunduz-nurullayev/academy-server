package org.nazarov.software.server.tool.details;

import java.io.File;

/**
 * Created by Shahin on 10/13/2016.
 */
public class ProjectDetails {
    public static final String PROJECT_NAME = "Source Code Academy";
    public static final String PROJECT_AUTHOR = "Nazarov Studio";
    public static final String PROJECT_URL = "https://api-nazarov.rhcloud.com";
    public static final String IMAGE_LOCATION ="C:/Users/Shahin/git/api/src/main/webapp/uploads/images";


}
