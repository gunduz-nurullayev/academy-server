package org.nazarov.software.server.hibernate.service;

import org.nazarov.software.server.hibernate.entity.Course;
import org.nazarov.software.server.hibernate.tool.DefaultDao;

/**
 * Created by Shahin on 11/3/2016.
 */
public interface CourseService extends DefaultDao<String , Course> {
}
