package org.nazarov.software.server.hibernate.service.impl;

import org.nazarov.software.server.hibernate.entity.User;
import org.nazarov.software.server.hibernate.entity.UserDetails;
import org.nazarov.software.server.hibernate.service.UserDetailsService;
import org.nazarov.software.server.hibernate.service.UserService;
import org.nazarov.software.server.hibernate.tool.DefaultDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Shahin on 10/18/2016.
 */
@Service
public class UserDetailsServiceImpl extends DefaultDaoImpl<Long , UserDetails> implements UserDetailsService{

}
