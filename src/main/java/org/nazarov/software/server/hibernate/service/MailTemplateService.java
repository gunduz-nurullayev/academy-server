package org.nazarov.software.server.hibernate.service;

import org.nazarov.software.server.hibernate.entity.MailTemplate;
import org.nazarov.software.server.hibernate.tool.DefaultDao;

/**
 * Created by Shahin on 10/17/2016.
 */
public interface MailTemplateService extends DefaultDao<String , MailTemplate> {
}
