package org.nazarov.software.server.hibernate.service;

import org.nazarov.software.server.hibernate.entity.Payment;
import org.nazarov.software.server.hibernate.tool.DefaultDao;

/**
 * Created by Shahin on 11/3/2016.
 */
public interface PaymentService extends DefaultDao<Long , Payment> {
}
