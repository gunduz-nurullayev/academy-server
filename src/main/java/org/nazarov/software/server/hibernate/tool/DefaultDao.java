package org.nazarov.software.server.hibernate.tool;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * Created by Shahin on 10/10/2016.
 */
public interface DefaultDao<PK extends Serializable, M> {

    M get(PK key);

    List<M> list();

    void save(M model);

    void update(M model);

    void remove(M model);

    int count();

    List<M> advanceSearch(SearchModel... values);

    List<M> advanceSearch(int limit,SearchModel... values);

    List<M> advanceSearch(String[] orders,SearchModel... values);

    List<M> advanceSearch(String[] orders, int limit,SearchModel... values);

    List<M> advanceSearch(String[] orders, int start, int limit,SearchModel... values);
}
