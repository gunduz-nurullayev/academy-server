package org.nazarov.software.server.hibernate.service.impl;

import org.nazarov.software.server.hibernate.entity.Payment;
import org.nazarov.software.server.hibernate.service.PaymentService;
import org.nazarov.software.server.hibernate.tool.DefaultDaoImpl;
import org.springframework.stereotype.Service;

/**
 * Created by Shahin on 11/3/2016.
 */
@Service
public class PaymentServiceImpl extends DefaultDaoImpl<Long , Payment> implements PaymentService {
}
