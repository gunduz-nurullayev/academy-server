package org.nazarov.software.server.hibernate.service;

import org.nazarov.software.server.hibernate.entity.Teacher;
import org.nazarov.software.server.hibernate.entity.UserDetails;
import org.nazarov.software.server.hibernate.tool.DefaultDao;

import java.util.List;

/**
 * Created by Shahin on 11/3/2016.
 */
public interface TeacherService extends DefaultDao<Long , Teacher> {
    List<UserDetails> getUserDetails();
    List<UserDetails> getUserDetails(String name);
}
