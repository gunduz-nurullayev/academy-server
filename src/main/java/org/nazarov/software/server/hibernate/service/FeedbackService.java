package org.nazarov.software.server.hibernate.service;

import org.nazarov.software.server.hibernate.entity.Feedback;
import org.nazarov.software.server.hibernate.tool.DefaultDao;

/**
 * Created by Shahin on 10/13/2016.
 */
public interface FeedbackService extends DefaultDao<Integer , Feedback> {

}
