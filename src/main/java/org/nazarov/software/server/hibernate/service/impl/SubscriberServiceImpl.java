package org.nazarov.software.server.hibernate.service.impl;

import org.nazarov.software.server.hibernate.entity.Subscriber;
import org.nazarov.software.server.hibernate.service.SubscriberService;
import org.nazarov.software.server.hibernate.tool.DefaultDaoImpl;
import org.springframework.stereotype.Service;

/**
 * Created by Shahin on 10/13/2016.
 */
@Service
public class SubscriberServiceImpl extends DefaultDaoImpl<Integer,Subscriber> implements SubscriberService{

    @Override
    public Subscriber checkUnsubscribe(String mail,String token) {
        return (Subscriber) getSession().getNamedQuery("subscribe.unsubscribe")
                .setParameter("mail" , mail).setParameter("token" ,token).uniqueResult();
    }

    @Override
    public Subscriber checkEmail(String mail) {
        return (Subscriber) getSession().getNamedQuery("subscribe.findByMail")
                .setParameter("mail" , mail).uniqueResult();
    }
}
