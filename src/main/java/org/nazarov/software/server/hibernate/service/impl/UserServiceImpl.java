package org.nazarov.software.server.hibernate.service.impl;

import org.nazarov.software.server.hibernate.entity.User;
import org.nazarov.software.server.hibernate.entity.UserDetails;
import org.nazarov.software.server.hibernate.service.UserDetailsService;
import org.nazarov.software.server.hibernate.service.UserService;
import org.nazarov.software.server.hibernate.tool.DefaultDaoImpl;
import org.nazarov.software.server.hibernate.tool.enums.UserMode;
import org.nazarov.software.server.spring.api.model.UserDetailsModel;
import org.shaheen.nazarov.tools.util.RandomValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;

/**
 * Created by Shahin on 10/10/2016.
 */
@Service
public class UserServiceImpl extends DefaultDaoImpl<Long, User> implements UserService {

    @Override
    public void save(User model) {
        UserDetails model2 = new UserDetails();
        model2.setCreated(new Date());
        model2.setMode(UserMode.ACTIVATED);
        model2.setToken(RandomValue.generate(30));
        model.setUserDetails(model2);
        super.save(model);
    }

    @Override
    public void update(User model) {
        model.getUserDetails().setToken(RandomValue.generate(30));
        super.update(model);
    }

    @Override
    public User findByMail(String mail) {
        List<User> list = getSession().getNamedQuery("user.findByMail").setParameter("mail", mail).list();
        if (list != null && list.size() > 0)
            return list.get(0);
        return null;
    }

    @Override
    public User findById(long id) {
        List<User> list = getSession().getNamedQuery("user.findById").setParameter("id", id).list();
        if (list != null && list.size() > 0)
            return list.get(0);
        return null;
    }

    @Override
    public User findByMailAndToken(String mail, String token) {
        List<User> list = getSession().getNamedQuery("user.findByMailAndToken")
                .setParameter("token", token).setParameter("mail", mail).list();
        if (list != null && list.size() > 0)
            return list.get(0);
        return null;
    }
}
