package org.nazarov.software.server.hibernate.tool.enums;

/**
 * Created by Shahin on 10/20/2016.
 */
public enum  UserMode {
    ACTIVATED,
    DEACTIVATED,
    FREEZED,
    FORGOT_PASSWORD_LINK_SEND
}
