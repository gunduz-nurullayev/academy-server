package org.nazarov.software.server.hibernate.tool.enums;

/**
 * Created by Shahin on 3/7/2017.
 */
public enum UserRole {
    USER,STUDENT,TEACHER,MANAGER,ADMIN
}
