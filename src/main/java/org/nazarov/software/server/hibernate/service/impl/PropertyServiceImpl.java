package org.nazarov.software.server.hibernate.service.impl;

import org.nazarov.software.server.hibernate.entity.Property;
import org.nazarov.software.server.hibernate.service.PropertyService;
import org.nazarov.software.server.hibernate.tool.DefaultDaoImpl;
import org.springframework.stereotype.Service;

/**
 * Created by Shahin on 10/16/2016.
 */
@Service
public class PropertyServiceImpl extends DefaultDaoImpl<String,Property> implements PropertyService {


}
