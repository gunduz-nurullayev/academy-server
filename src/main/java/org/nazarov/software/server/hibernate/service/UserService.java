package org.nazarov.software.server.hibernate.service;

import org.nazarov.software.server.hibernate.entity.User;
import org.nazarov.software.server.hibernate.tool.DefaultDao;

/**
 * Created by Shahin on 10/10/2016.
 */
public interface UserService extends DefaultDao<Long, User> {
    User findByMail(String mail);
    User findById(long id);
    User findByMailAndToken(String mail, String token);

}
