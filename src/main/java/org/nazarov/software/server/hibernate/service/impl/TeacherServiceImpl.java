package org.nazarov.software.server.hibernate.service.impl;

import org.nazarov.software.server.hibernate.entity.Teacher;
import org.nazarov.software.server.hibernate.entity.UserDetails;
import org.nazarov.software.server.hibernate.service.TeacherService;
import org.nazarov.software.server.hibernate.tool.DefaultDaoImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Shahin on 11/3/2016.
 */
@Service
public class TeacherServiceImpl extends DefaultDaoImpl<Long , Teacher> implements TeacherService{
    @Override
    public List<UserDetails> getUserDetails() {
        return getSession().getNamedQuery("teacher.userDetails").list();
    }

    @Override
    public List<UserDetails> getUserDetails(String name) {
        return getSession().getNamedQuery("teacher.userDetails").setParameter("name",name).list();
    }
}
