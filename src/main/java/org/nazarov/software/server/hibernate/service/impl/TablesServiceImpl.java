package org.nazarov.software.server.hibernate.service.impl;

import org.nazarov.software.server.hibernate.entity.Tables;
import org.nazarov.software.server.hibernate.service.TablesService;
import org.nazarov.software.server.hibernate.tool.DefaultDaoImpl;
import org.springframework.stereotype.Service;

/**
 * Created by Shahin on 11/3/2016.
 */
@Service
public class TablesServiceImpl extends DefaultDaoImpl<Long , Tables> implements TablesService{

}
