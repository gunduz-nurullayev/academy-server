package org.nazarov.software.server.hibernate.service.impl;

import org.nazarov.software.server.hibernate.entity.Student;
import org.nazarov.software.server.hibernate.service.StudentService;
import org.nazarov.software.server.hibernate.tool.DefaultDaoImpl;
import org.springframework.stereotype.Service;

/**
 * Created by Shahin on 11/3/2016.
 */
@Service
public class StudentServiceImpl extends DefaultDaoImpl<Long , Student> implements StudentService{
}
