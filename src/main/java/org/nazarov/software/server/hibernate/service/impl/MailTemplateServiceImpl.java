package org.nazarov.software.server.hibernate.service.impl;

import org.nazarov.software.server.hibernate.entity.MailTemplate;
import org.nazarov.software.server.hibernate.service.MailTemplateService;
import org.nazarov.software.server.hibernate.tool.DefaultDaoImpl;
import org.springframework.stereotype.Service;

/**
 * Created by Shahin on 10/17/2016.
 */
@Service
public class MailTemplateServiceImpl extends DefaultDaoImpl<String , MailTemplate> implements MailTemplateService {

}
