package org.nazarov.software.server.hibernate.service.impl;

import org.nazarov.software.server.hibernate.entity.Activity;
import org.nazarov.software.server.hibernate.service.ActivityService;
import org.nazarov.software.server.hibernate.tool.DefaultDaoImpl;
import org.springframework.stereotype.Service;

/**
 * Created by Shahin on 12/17/2016.
 */
@Service
public class ActivityServiceImpl extends DefaultDaoImpl<Long , Activity> implements ActivityService {

}
