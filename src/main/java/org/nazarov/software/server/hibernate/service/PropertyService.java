package org.nazarov.software.server.hibernate.service;

import org.nazarov.software.server.hibernate.entity.Property;
import org.nazarov.software.server.hibernate.tool.DefaultDao;

/**
 * Created by Shahin on 10/16/2016.
 */
public interface PropertyService extends DefaultDao<String , Property> {
}
