package org.nazarov.software.server.hibernate.service;

import org.nazarov.software.server.hibernate.entity.Group;
import org.nazarov.software.server.hibernate.tool.DefaultDao;

/**
 * Created by Shahin on 11/3/2016.
 */
public interface GroupService extends DefaultDao<String , Group> {
}
