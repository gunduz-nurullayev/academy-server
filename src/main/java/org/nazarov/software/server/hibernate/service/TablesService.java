package org.nazarov.software.server.hibernate.service;

import org.nazarov.software.server.hibernate.entity.Tables;
import org.nazarov.software.server.hibernate.tool.DefaultDao;

/**
 * Created by Shahin on 11/3/2016.
 */
public interface TablesService extends DefaultDao<Long , Tables> {
}
