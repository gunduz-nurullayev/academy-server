package org.nazarov.software.server.hibernate.service;

import org.nazarov.software.server.hibernate.entity.Activity;
import org.nazarov.software.server.hibernate.tool.DefaultDao;

/**
 * Created by Shahin on 12/17/2016.
 */
public interface ActivityService extends DefaultDao<Long,Activity> {
}
