package org.nazarov.software.server.hibernate.service.impl;

import org.nazarov.software.server.hibernate.entity.Course;
import org.nazarov.software.server.hibernate.service.CourseService;
import org.nazarov.software.server.hibernate.tool.DefaultDaoImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Shahin on 11/3/2016.
 */
@Service
public class CourseServiceImpl extends DefaultDaoImpl<String , Course> implements CourseService {

}
