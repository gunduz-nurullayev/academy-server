package org.nazarov.software.server.hibernate.service;

import org.nazarov.software.server.hibernate.entity.User;
import org.nazarov.software.server.hibernate.entity.UserDetails;
import org.nazarov.software.server.hibernate.tool.DefaultDao;

/**
 * Created by Shahin on 10/18/2016.
 */
public interface UserDetailsService extends DefaultDao<Long , UserDetails> {
}
