package org.nazarov.software.server.hibernate.service.impl;

import org.nazarov.software.server.hibernate.entity.Lessons;
import org.nazarov.software.server.hibernate.service.LessonsService;
import org.nazarov.software.server.hibernate.tool.DefaultDaoImpl;
import org.springframework.stereotype.Service;

/**
 * Created by Shahin on 11/3/2016.
 */
@Service
public class LessonsServiceImpl extends DefaultDaoImpl<Long , Lessons> implements LessonsService {
}
