package org.nazarov.software.server.hibernate.service;

import org.nazarov.software.server.hibernate.entity.Subscriber;
import org.nazarov.software.server.hibernate.tool.DefaultDao;

/**
 * Created by Shahin on 10/13/2016.
 */
public interface SubscriberService extends DefaultDao<Integer , Subscriber> {
    Subscriber checkUnsubscribe(String mail, String token);
    Subscriber checkEmail(String mail);
}
