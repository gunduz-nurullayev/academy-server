package org.nazarov.software.server.hibernate.service.impl;

import org.nazarov.software.server.hibernate.entity.Group;
import org.nazarov.software.server.hibernate.service.GroupService;
import org.nazarov.software.server.hibernate.tool.DefaultDaoImpl;
import org.springframework.stereotype.Service;

/**
 * Created by Shahin on 11/3/2016.
 */
@Service
public class GroupServiceImpl extends DefaultDaoImpl<String , Group> implements GroupService{
}
