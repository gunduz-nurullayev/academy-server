package org.nazarov.software.server.hibernate.service.impl;

import org.nazarov.software.server.hibernate.entity.Image;
import org.nazarov.software.server.hibernate.service.ImageService;
import org.nazarov.software.server.hibernate.tool.DefaultDao;
import org.nazarov.software.server.hibernate.tool.DefaultDaoImpl;
import org.springframework.stereotype.Service;

/**
 * Created by Shahin on 3/20/2017.
 */
@Service
public class ImageServiceImpl extends DefaultDaoImpl<String,Image> implements ImageService {
}
