package org.nazarov.software.server.hibernate.service;

import org.nazarov.software.server.hibernate.entity.Billing;
import org.nazarov.software.server.hibernate.tool.DefaultDao;

/**
 * Created by Shahin on 3/20/2017.
 */
public interface BillingService extends DefaultDao<Long,Billing>{
}
