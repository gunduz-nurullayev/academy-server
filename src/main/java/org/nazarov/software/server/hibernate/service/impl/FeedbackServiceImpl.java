package org.nazarov.software.server.hibernate.service.impl;

import org.nazarov.software.server.hibernate.entity.Feedback;
import org.nazarov.software.server.hibernate.service.FeedbackService;
import org.nazarov.software.server.hibernate.tool.DefaultDaoImpl;
import org.springframework.stereotype.Service;

/**
 * Created by Shahin on 10/13/2016.
 */
@Service
public class FeedbackServiceImpl extends DefaultDaoImpl<Integer , Feedback> implements FeedbackService {

}
