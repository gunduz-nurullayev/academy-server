package org.nazarov.software.server.hibernate.service.impl;

import org.nazarov.software.server.hibernate.entity.Billing;
import org.nazarov.software.server.hibernate.service.BillingService;
import org.nazarov.software.server.hibernate.tool.DefaultDaoImpl;
import org.springframework.stereotype.Service;

/**
 * Created by Shahin on 3/20/2017.
 */
@Service
public class BillingServiceImpl extends DefaultDaoImpl<Long,Billing> implements BillingService {
}
