package org.nazarov.software.server.spring.configuration;

import com.mchange.v2.c3p0.C3P0Registry;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.mysql.jdbc.Driver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.jndi.JndiObjectFactoryBean;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created by Shahin on 10/8/2016.
 */
@Configuration
@PropertySources(@PropertySource("classpath:/META-INF/configuration/hibernate.properties"))
@ComponentScan(basePackages = "org.nazarov.software.server.hibernate")
public class HibernateConfiguration {

    @Autowired
    private Environment environment;

//    @Bean
//    public JndiObjectFactoryBean getJndiObjectFactoryBean(){
//        JndiObjectFactoryBean factoryBean = new JndiObjectFactoryBean();
//        factoryBean.setJndiName("java:comp/env/jdbc/MySQLDS");
//        factoryBean.setLookupOnStartup(true);
//        factoryBean.setProxyInterface(DataSource.class);
//        return factoryBean;
//    }

    @Bean
    public DataSource dataSource() throws SQLException {
        return new SimpleDriverDataSource(new Driver(),"jdbc:mysql://localhost:3309/source_code","root","0000");
    }

    @Bean("sessionFactory")
    public LocalSessionFactoryBean sessionFactoryBean() throws SQLException {
        LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();
        sessionFactoryBean.setPackagesToScan("org.nazarov.software.server.hibernate.entity");
//        sessionFactoryBean.setDataSource((DataSource) getJndiObjectFactoryBean().getObject());
        sessionFactoryBean.setDataSource(dataSource());
        sessionFactoryBean.setHibernateProperties(hibernateProperties());
        return sessionFactoryBean;
    }

    private Properties hibernateProperties() {
        Properties properties = new Properties();
        properties.setProperty("hibernate.dialect", environment.getProperty("hibernate.dialect"));
        properties.setProperty("hibernate.show_sql", environment.getProperty("hibernate.show_sql"));
        properties.setProperty("hibernate.format_sql", environment.getProperty("hibernate.format_sql"));
        properties.setProperty("hibernate.hbm2ddl.auto", environment.getProperty("hibernate.hbm2ddl.auto"));
//        properties.setProperty("current_session_context_class", environment.getProperty("current_session_context_class"));
//        properties.setProperty("hibernate.connection.datasource", environment.getProperty("hibernate.connection.datasource"));
//        properties.setProperty("hibernate.connection.driver_class", environment.getProperty("hibernate.connection.driver_class"));
//        properties.setProperty("hibernate.connection.url", environment.getProperty("hibernate.connection.url"));
//        properties.setProperty("hibernate.connection.username", environment.getProperty("hibernate.connection.username"));
//        properties.setProperty("hibernate.connection.password", environment.getProperty("hibernate.connection.password"));




//        properties.setProperty("hibernate.connection.shutdown", environment.getProperty("hibernate.connection.shutdown"));
        properties.setProperty("hibernate.cache.use_second_level_cache", environment.getProperty("hibernate.cache.use_second_level_cache"));
        properties.setProperty("hibernate.cache.use_query_cache", environment.getProperty("hibernate.cache.use_query_cache"));
        properties.setProperty("hibernate.cache.provider_class", environment.getProperty("hibernate.cache.provider_class"));
        properties.setProperty("hibernate.use_sql_comments", environment.getProperty("hibernate.use_sql_comments"));
//        properties.setProperty("hibernate.default_schema", environment.getProperty("hibernate.default_schema"));















//        properties.setProperty("cache.provider_class", environment.getProperty("cache.provider_class"));
//        properties.setProperty("hibernate.max_fetch_depth", environment.getProperty("hibernate.max_fetch_depth"));

//        properties.setProperty("hibernate.connection.provider_class", environment.getProperty("hibernate.connection.provider_class"));
//        properties.setProperty("hibernate.c3p0.min_size", environment.getProperty("hibernate.c3p0.min_size"));
//        properties.setProperty("hibernate.c3p0.max_size", environment.getProperty("hibernate.c3p0.max_size"));
//        properties.setProperty("hibernate.c3p0.acquire_increment", environment.getProperty("hibernate.c3p0.acquire_increment"));
//        properties.setProperty("hibernate.c3p0.idle_test_period", environment.getProperty("hibernate.c3p0.idle_test_period"));
//        properties.setProperty("hibernate.c3p0.max_statements", environment.getProperty("hibernate.c3p0.max_statements"));
//        properties.setProperty("hibernate.c3p0.preferredTestQuery", environment.getProperty("hibernate.c3p0.preferredTestQuery"));
//        properties.setProperty("hibernate.c3p0.testConnectionOnCheckout", environment.getProperty("hibernate.c3p0.testConnectionOnCheckout"));
//        properties.setProperty("hibernate.c3p0.timeout", environment.getProperty("hibernate.c3p0.timeout"));
//        properties.setProperty("hibernate.c3p0.validate", environment.getProperty("hibernate.c3p0.validate"));

        return properties;
    }

}
