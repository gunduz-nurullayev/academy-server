package org.nazarov.software.server.spring.api.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.nazarov.software.server.hibernate.entity.Image;
import org.nazarov.software.server.hibernate.entity.UserDetails;
import org.nazarov.software.server.hibernate.service.ImageService;
import org.nazarov.software.server.hibernate.tool.enums.Gender;
import org.nazarov.software.server.hibernate.tool.enums.UserMode;
import org.nazarov.software.server.spring.util.ModelBuilderImpl;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by Shahin on 11/3/2016.
 */
public class UserDetailsModel extends ModelBuilderImpl<UserDetailsModel , UserDetails> {

    private ImageService imageService;

    public UserDetailsModel(ImageService imageService) {
        this.imageService = imageService;
    }

    public UserDetailsModel() {
    }

    @NotNull
    private long id;
    private UserMode mode;
    private String fullName;
    private String details;
    @NotNull
    private Gender gender;
    private String imageName;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'" ,locale = "US")
    private Date created;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'" ,locale = "US")
    private Date lastAccess;
    private String token;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public UserMode getMode() {
        return mode;
    }

    public void setMode(UserMode mode) {
        this.mode = mode;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastAccess() {
        return lastAccess;
    }

    public void setLastAccess(Date lastAccess) {
        this.lastAccess = lastAccess;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    @Override
    public UserDetailsModel convertToModel(UserDetails entity) {
        UserDetailsModel model = new UserDetailsModel();
        model.setId(entity.getId());
        model.setCreated(entity.getCreated());
        model.setFullName(entity.getFullName());
        model.setDetails(entity.getDetails());
        model.setLastAccess(entity.getLastAccess());
        model.setMode(entity.getMode());
        model.setGender(entity.getGender());
        if(entity.getImage() != null){
            model.setImageName(entity.getImage().getName());
        }
        model.setToken(entity.getToken());
        return model;
    }

    @Override
    public UserDetails convertToEntity(UserDetails entity, UserDetailsModel model) {
        entity.setCreated(model.getCreated());
        entity.setFullName(model.getFullName());
        entity.setLastAccess(model.getLastAccess());
        entity.setMode(model.getMode());
        entity.setGender(model.getGender());
        entity.setDetails(model.getDetails());
        Image image = imageService.get(model.getImageName());
        if(image != null){
            entity.setImage(image);
        }
        entity.setToken(model.getToken());
        return entity;
    }
}
