package org.nazarov.software.server.spring.api;

import org.nazarov.software.server.hibernate.entity.Subscriber;
import org.nazarov.software.server.hibernate.service.SubscriberService;
import org.nazarov.software.server.spring.api.model.SearchFilterModel;
import org.nazarov.software.server.spring.api.model.SubscriberModel;
import org.nazarov.software.server.spring.util.ViewBuilderImpl;
import org.nazarov.software.server.tool.Server;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Shahin on 1/4/2017.
 */
@RestController
@RequestMapping(Server.API + Server.SUBSCRIBER)
public class SubscriberRestController extends ViewBuilderImpl<SubscriberModel , Integer> {

    public SubscriberRestController(@Autowired SubscriberService service) {
        super(new SubscriberModel() , service);
    }

    @Override
    public ResponseEntity<Void> create(@RequestBody @Valid SubscriberModel model, UriComponentsBuilder ucBuilder) {
        return super.create(new Subscriber() , model ,Server.SUBSCRIBER, ucBuilder, model.getId());
    }

    @Override
    public ResponseEntity<List<SubscriberModel>> list() {
        return super.list();
    }

    @Override
    public ResponseEntity<SubscriberModel> get(@PathVariable("id") Integer key) {
        return super.get(key);
    }

    @Override
    public ResponseEntity<SubscriberModel> update(@PathVariable("id") Integer id,@RequestBody @Valid SubscriberModel model) {
        return super.update(id, model);
    }

    @Override
    public ResponseEntity<SubscriberModel> delete(@PathVariable("id") Integer id) {
        return super.delete(id);
    }

    @Override
    public ResponseEntity<List<SubscriberModel>> search(@RequestBody @Valid SearchFilterModel searchModel) {
        return super.search(searchModel);
    }
}
