package org.nazarov.software.server.spring.api.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.nazarov.software.server.hibernate.entity.Course;
import org.nazarov.software.server.hibernate.entity.Group;
import org.nazarov.software.server.hibernate.entity.Student;
import org.nazarov.software.server.hibernate.entity.Teacher;
import org.nazarov.software.server.hibernate.service.CourseService;
import org.nazarov.software.server.hibernate.service.StudentService;
import org.nazarov.software.server.hibernate.service.TeacherService;
import org.nazarov.software.server.spring.util.ModelBuilderImpl;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Shahin on 11/3/2016.
 */
public class GroupModel extends ModelBuilderImpl<GroupModel , Group> {

    private CourseService courseService;
    private StudentService studentService;
    private TeacherService teacherService;

    public GroupModel(CourseService courseService, StudentService studentService, TeacherService teacherService) {
        this.courseService = courseService;
        this.studentService = studentService;
        this.teacherService = teacherService;
    }

    public GroupModel() {
    }

    private String name;
    private long teacher;
    private String course;
    private boolean enabled;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'" ,locale = "US")
    private Date start;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'" ,locale = "US")
    private Date finish;
    private Set<Long> students;

    public Set<Long> getStudents() {
        return students;
    }

    public void setStudents(Set<Long> students) {
        this.students = students;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getTeacher() {
        return teacher;
    }

    public void setTeacher(long teacher) {
        this.teacher = teacher;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getFinish() {
        return finish;
    }

    public void setFinish(Date finish) {
        this.finish = finish;
    }



    public GroupModel convertToModel(Group group){
        if(group == null)
            return null;
        GroupModel model = new GroupModel();
        model.setName(group.getName());
        model.setEnabled(group.isEnabled());
        if(group.getTeacher() != null) {
            model.setTeacher(group.getTeacher().getId());
        }
        if(group.getCourse() != null) {
            model.setCourse(group.getCourse().getName());
        }
        Set<Long> students = new HashSet<Long>();
        for(Student student : group.getStudents()){
            students.add(student.getId());
        }
        model.setStudents(students);
        model.setStart(group.getStart());
        model.setFinish(group.getFinish());
        return model;
    }

    public Group convertToEntity(Group group, GroupModel model){
        group.setFinish(model.getFinish());
        group.setStart(model.getStart());
        group.setEnabled(model.isEnabled());
        Course course = courseService.get(model.getCourse());
        if(course != null){
            group.setCourse(course);
        }
        Teacher teacher = teacherService.get(model.getTeacher());
        if(teacher != null){
            group.setTeacher(teacher);
        }
        Set<Student> students = new HashSet<Student>();
        for(long id : model.getStudents()){
            Student student = studentService.get(id);
            if(student != null){
                students.add(student);
            }
        }
        group.setStudents(students);
        return group;
    }
}
