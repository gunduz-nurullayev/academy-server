package org.nazarov.software.server.spring.api.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.nazarov.software.server.hibernate.entity.Group;
import org.nazarov.software.server.hibernate.entity.Tables;
import org.nazarov.software.server.hibernate.service.GroupService;
import org.nazarov.software.server.spring.util.ModelBuilderImpl;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by Shahin on 11/3/2016.
 */
public class TableModel extends ModelBuilderImpl<TableModel , Tables>{

    private long id;
    @NotNull
    private String group;
    @NotNull
    private int dayOfWeek;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'" ,locale = "US")
    @NotNull
    private Date time;
    @NotNull
    private int hours;
    @NotNull
    private boolean enabled;

    private GroupService service;

    public TableModel(GroupService service) {
        this.service = service;
    }

    public TableModel() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public int getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(int dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public TableModel convertToModel(Tables entity) {
        TableModel tableModel = new TableModel();
        tableModel.setId(entity.getId());
        tableModel.setEnabled(entity.isEnabled());
        if(entity.getGroup() == null ){
            tableModel.setGroup(entity.getGroup().getName());
        }
        tableModel.setDayOfWeek(entity.getDayOfWeek());
        tableModel.setHours(entity.getHours());
        tableModel.setTime(entity.getTime());
        return tableModel;
    }

    @Override
    public Tables convertToEntity(Tables entity, TableModel model) {
        entity.setEnabled(model.isEnabled());
        Group group = service.get(model.getGroup());
        if(group != null){
            entity.setGroup(group);
        }
        entity.setDayOfWeek(model.getDayOfWeek());
        entity.setHours(model.getHours());
        entity.setTime(model.getTime());
        return entity;
    }
}
