package org.nazarov.software.server.spring.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shahin on 12/17/2016.
 */
public abstract class ModelBuilderImpl<M, E> implements ModelBuilder<M, E> {

    @Override
    public List<M> convertToModel(List<E> entities) {
        List<M> models = new ArrayList<M>();
        for (E entity : entities) {
            models.add(convertToModel(entity));
        }
        return models;
    }
}
