package org.nazarov.software.server.spring.api;

import org.nazarov.software.server.hibernate.entity.Activity;
import org.nazarov.software.server.hibernate.entity.Student;
import org.nazarov.software.server.hibernate.entity.User;
import org.nazarov.software.server.hibernate.service.ActivityService;
import org.nazarov.software.server.hibernate.service.GroupService;
import org.nazarov.software.server.hibernate.service.StudentService;
import org.nazarov.software.server.hibernate.service.UserService;
import org.nazarov.software.server.hibernate.tool.SearchModel;
import org.nazarov.software.server.hibernate.tool.enums.SearchAttribute;
import org.nazarov.software.server.spring.api.model.ActivityModel;
import org.nazarov.software.server.spring.api.model.SearchFilterModel;
import org.nazarov.software.server.spring.util.ViewBuilderImpl;
import org.nazarov.software.server.tool.Server;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.security.Principal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Shahin on 12/17/2016.
 */
@RestController
@RequestMapping(Server.API + Server.ACTIVITY)
public class ActivityRestController extends ViewBuilderImpl<ActivityModel, Long> {

    @Autowired
    private UserService userService;
    @Autowired
    private StudentService studentService;

    public ActivityRestController(@Autowired GroupService groupService, @Autowired StudentService studentService, @Autowired ActivityService activityService) {
        super(new ActivityModel(groupService, studentService), activityService);
    }

    @Override
    public ResponseEntity<List<ActivityModel>> list() {
        return super.list();
    }

    @RequestMapping(value = "info",method = RequestMethod.GET,
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<List<ActivityModel>> myList(Principal principal) {
        User user = userService.findByMail(principal.getName());
        Student student = studentService.get(user.getId());
        if(user != null){
            SearchModel searchModel =new SearchModel("student", SearchAttribute.EQUALS,false,student);
            List<ActivityModel> models = getModelBuilder().convertToModel(getDefaultDao().advanceSearch(new String[]{"date desc"},searchModel));
            if (models == null || models.isEmpty()) {
                return noContent();
            }
            return okList(models);
        }
        return noContent();
    }

    @Override
    public ResponseEntity<ActivityModel> get(@PathVariable("id") Long id) {
        return super.get(id);
    }

    @Override
    public ResponseEntity<Void> create(@RequestBody @Valid ActivityModel model, UriComponentsBuilder ucBuilder) {
        return super.create(new Activity(), model, Server.ACTIVITY, ucBuilder, model.getId());
    }

    @Override
    public ResponseEntity<ActivityModel> update(@PathVariable("id") Long id, @RequestBody @Valid ActivityModel model) {
        return super.update(id, model);
    }

    @Override
    public ResponseEntity<ActivityModel> delete(@PathVariable("id") Long id) {
        return super.delete(id);
    }

    @Override
    public ResponseEntity<List<ActivityModel>> search(@RequestBody @Valid SearchFilterModel searchModel) {
        return super.search(searchModel);
    }

}
