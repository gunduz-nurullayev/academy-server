package org.nazarov.software.server.spring.api;

import org.nazarov.software.server.hibernate.entity.Course;
import org.nazarov.software.server.hibernate.service.CourseService;
import org.nazarov.software.server.hibernate.service.ImageService;
import org.nazarov.software.server.hibernate.service.TeacherService;
import org.nazarov.software.server.spring.api.model.CourseModel;
import org.nazarov.software.server.spring.api.model.SearchFilterModel;
import org.nazarov.software.server.spring.util.ViewBuilderImpl;
import org.nazarov.software.server.tool.Server;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Shahin on 11/7/2016.
 */
@RestController
@RequestMapping(Server.API + Server.COURSE)
public class CourseRestController extends ViewBuilderImpl<CourseModel, String> {


    public CourseRestController(@Autowired CourseService courseService
            ,@Autowired ImageService imageService, @Autowired TeacherService teacherService) {
        super(new CourseModel(teacherService,imageService), courseService);
    }

    public ResponseEntity<List<CourseModel>> list() {
        return super.list();
    }


    public ResponseEntity<CourseModel> get(@PathVariable("id") String id) {
        return super.get(id);
    }


    public ResponseEntity<Void> create(@RequestBody @Valid CourseModel model, UriComponentsBuilder ucBuilder) {
        Course course = new Course();
        course.setName(model.getName());
        return super.create(course, model, Server.COURSE, ucBuilder, model.getName());
    }

    public ResponseEntity<CourseModel> update(@PathVariable("id") String id, @RequestBody @Valid CourseModel model) {
        return super.update(id, model);
    }


    public ResponseEntity<CourseModel> delete(@PathVariable("id") String id) {
        return super.delete(id);
    }

    public ResponseEntity<List<CourseModel>> search(
            @RequestBody @Valid SearchFilterModel searchModel
    ) {
        return super.search(searchModel);
    }

}
