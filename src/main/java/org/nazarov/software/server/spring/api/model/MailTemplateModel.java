package org.nazarov.software.server.spring.api.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.nazarov.software.server.hibernate.entity.MailTemplate;
import org.nazarov.software.server.spring.util.ModelBuilderImpl;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by Shahin on 11/3/2016.
 */
public class MailTemplateModel extends ModelBuilderImpl<MailTemplateModel , MailTemplate> {

    @NotNull
    private String name;
    @NotNull
    private String htmlText;
    private boolean htmlMode;
    private String subject;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'" ,locale = "US")
    private Date registred = new Date();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHtmlText() {
        return htmlText;
    }

    public void setHtmlText(String htmlText) {
        this.htmlText = htmlText;
    }

    public boolean isHtmlMode() {
        return htmlMode;
    }

    public void setHtmlMode(boolean htmlMode) {
        this.htmlMode = htmlMode;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Date getRegistred() {
        return registred;
    }

    public void setRegistred(Date registred) {
        this.registred = registred;
    }

    @Override
    public MailTemplateModel convertToModel(MailTemplate entity) {
        if(entity == null)
            return null;
        MailTemplateModel model = new MailTemplateModel();
        model.setName(entity.getName());
        model.setHtmlMode(entity.isHtmlMode());
        model.setHtmlText(entity.getHtmlText());
        model.setRegistred(entity.getRegistred());
        model.setSubject(entity.getSubject());
        return model;
    }

    @Override
    public MailTemplate convertToEntity(MailTemplate entity, MailTemplateModel model) {
        entity.setHtmlMode(model.isHtmlMode());
        entity.setHtmlText(model.getHtmlText());
        entity.setRegistred(model.getRegistred());
        entity.setSubject(model.getSubject());
        return entity;
    }
}
