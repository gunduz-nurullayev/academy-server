package org.nazarov.software.server.spring.api.model;

import org.nazarov.software.server.hibernate.entity.Course;
import org.nazarov.software.server.hibernate.entity.Lessons;
import org.nazarov.software.server.hibernate.entity.Student;
import org.nazarov.software.server.hibernate.service.CourseService;
import org.nazarov.software.server.hibernate.service.StudentService;
import org.nazarov.software.server.hibernate.service.TeacherService;
import org.nazarov.software.server.spring.util.ModelBuilderImpl;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Shahin on 11/3/2016.
 */
public class LessonModel extends ModelBuilderImpl<LessonModel, Lessons> {

    private CourseService courseService;

    public LessonModel(CourseService courseService) {
        this.courseService = courseService;
    }

    public LessonModel() {
    }

    private long id;

    private String name;

    private String description;

    private String course;

    private int period;

    private boolean enabled;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public LessonModel convertToModel(Lessons entity) {
        if(entity == null)
            return null;
        LessonModel model = new LessonModel();
        model.setId(entity.getId());
        model.setName(entity.getName());
        model.setEnabled(entity.isEnabled());
        model.setDescription(entity.getDescription());
        model.setPeriod(entity.getPeriod());
        if(entity.getCourse() != null) {
            model.setCourse(entity.getCourse().getName());
        }
        return model;
    }

    @Override
    public Lessons convertToEntity(Lessons entity, LessonModel model) {
        entity.setName(model.getName());
        entity.setEnabled(model.isEnabled());
        entity.setDescription(model.getDescription());
        entity.setPeriod(model.getPeriod());
        Course course = courseService.get(model.getCourse());
        if(course != null) {
            entity.setCourse(course);
        }
        return entity;
    }
}
