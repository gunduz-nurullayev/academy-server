package org.nazarov.software.server.spring.api;

import org.nazarov.software.server.hibernate.entity.Group;
import org.nazarov.software.server.hibernate.service.CourseService;
import org.nazarov.software.server.hibernate.service.GroupService;
import org.nazarov.software.server.hibernate.service.StudentService;
import org.nazarov.software.server.hibernate.service.TeacherService;
import org.nazarov.software.server.spring.api.model.GroupModel;
import org.nazarov.software.server.spring.api.model.SearchFilterModel;
import org.nazarov.software.server.spring.util.ViewBuilderImpl;
import org.nazarov.software.server.tool.Server;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

/**
 * Created by Shahin on 11/7/2016.
 */
@RestController
@RequestMapping(Server.API + Server.GROUP)
public class GroupRestController  extends ViewBuilderImpl<GroupModel, String> {


    public GroupRestController(@Autowired GroupService service,
                               @Autowired CourseService courseService,
                               @Autowired StudentService studentService,
                               @Autowired TeacherService teacherService) {
        super(new GroupModel(courseService , studentService ,teacherService), service);
    }


    @Override
    public ResponseEntity<List<GroupModel>> list() {
        return super.list();
    }

    @Override
    public ResponseEntity<GroupModel> get(@PathVariable("id") String id) {
        return super.get(id);
    }

    @Override
    public ResponseEntity<Void> create(@RequestBody GroupModel model, UriComponentsBuilder ucBuilder) {
        Group group = new Group();
        group.setName(model.getName());
        return super.create(group , model ,Server.GROUP, ucBuilder, model.getName());
    }

    @Override
    public ResponseEntity<GroupModel> update(@PathVariable("id") String id, @RequestBody GroupModel model) {
        return super.update(id , model);
    }

    @Override
    public ResponseEntity<GroupModel> delete(@PathVariable("id") String id) {
        return super.delete(id);
    }

    @Override
    public ResponseEntity<List<GroupModel>> search(@RequestBody SearchFilterModel searchModel) {
        return super.search(searchModel);
    }

}
