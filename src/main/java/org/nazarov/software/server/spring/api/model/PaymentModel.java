package org.nazarov.software.server.spring.api.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.nazarov.software.server.hibernate.entity.Group;
import org.nazarov.software.server.hibernate.entity.Payment;
import org.nazarov.software.server.hibernate.entity.Student;
import org.nazarov.software.server.hibernate.entity.User;
import org.nazarov.software.server.hibernate.service.GroupService;
import org.nazarov.software.server.hibernate.service.StudentService;
import org.nazarov.software.server.hibernate.service.UserService;
import org.nazarov.software.server.spring.util.ModelBuilderImpl;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by Shahin on 11/3/2016.
 */
public class PaymentModel extends ModelBuilderImpl<PaymentModel,Payment>{

    private long id;
    private String group;
    @NotNull
    private long student;
    @NotNull
    private long user;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'" ,locale = "US")
    @NotNull
    private Date paymentDate = new Date();
    @NotNull
    private double amount = 0.00;


    private GroupService groupService;
    private StudentService studentService;
    private UserService userService;

    public PaymentModel(GroupService groupService, StudentService studentService, UserService userService) {
        this.groupService = groupService;
        this.studentService = studentService;
        this.userService = userService;
    }

    public PaymentModel() {
    }

    @Override
    public PaymentModel convertToModel(Payment entity) {
        PaymentModel model = new PaymentModel();
        model.setId(entity.getId());
        model.setAmount(entity.getAmount());
        model.setPaymentDate(entity.getPaymentDate());
        if(entity.getGroup() != null){
            model.setGroup(entity.getGroup().getName());
        }
        if(entity.getStudent() != null){
            model.setStudent(entity.getStudent().getId());
        }
        if(entity.getUser() != null){
            model.setUser(entity.getUser().getId());
        }
        return model;
    }

    @Override
    public Payment convertToEntity(Payment entity, PaymentModel model) {
        entity.setAmount(model.getAmount());
        entity.setPaymentDate(model.getPaymentDate());
        Group group = groupService.get(model.getGroup());
        if(group != null){
            entity.setGroup(group);
        }
        Student student = studentService.get(model.getId());
        if(student != null){
            entity.setStudent(student);
        }
        User user = userService.get(model.getUser());
        if(user != null){
            entity.setUser(user);
        }
        return entity;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public long getStudent() {
        return student;
    }

    public void setStudent(long student) {
        this.student = student;
    }

    public long getUser() {
        return user;
    }

    public void setUser(long user) {
        this.user = user;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }


}
