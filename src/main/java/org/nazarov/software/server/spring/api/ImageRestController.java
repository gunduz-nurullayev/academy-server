package org.nazarov.software.server.spring.api;

import org.nazarov.software.server.hibernate.entity.Image;
import org.nazarov.software.server.hibernate.service.ImageService;
import org.nazarov.software.server.spring.api.model.ImageModel;
import org.nazarov.software.server.spring.api.model.SearchFilterModel;
import org.nazarov.software.server.tool.Server;
import org.nazarov.software.server.tool.details.ProjectDetails;
import org.shaheen.nazarov.tools.util.RandomValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * Created by Shahin on 3/20/2017.
 */
@RestController
@RequestMapping(Server.API + Server.IMAGE)
public class ImageRestController {

    private ImageModel model = new ImageModel();

    @Autowired
    private ImageService imageService;

    @RequestMapping(method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<List<ImageModel>> list() {
        List<Image> images = imageService.list();
        if (images != null && images.size() > 0) {
            return ResponseEntity.ok(this.model.convertToModel(images));
        }
        return new ResponseEntity<List<ImageModel>>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<ImageModel> get(@RequestParam("id") String key) {
        Image model = imageService.get(key);
        if (model != null) {
            return ResponseEntity.ok(this.model.convertToModel(model));
        }
        return new ResponseEntity<ImageModel>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/", method = RequestMethod.DELETE, consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<Void> delete(@RequestParam("id") String id, HttpServletRequest request) {
        Image model = imageService.get(id);
        if (model != null) {
            File file = new File(ProjectDetails.IMAGE_LOCATION + File.separator + id);
            file.delete();
            imageService.remove(model);
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.notFound().build();
    }

    @RequestMapping(value = "/filter", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<List<ImageModel>> search(@RequestBody @Valid SearchFilterModel searchModel) {
        List<Image> images = imageService.advanceSearch(searchModel.getOrders(), searchModel.getFirstResult(), searchModel.getMaxResult(), searchModel.getSearchModels());

        if (images != null && images.size() > 0) {
            return ResponseEntity.ok(this.model.convertToModel(images));
        }
        return new ResponseEntity<List<ImageModel>>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public
    @ResponseBody
    String handleFile(@RequestParam("file") MultipartFile file,
                      @RequestParam(value = "default", required = false, defaultValue = "") String isDefault,
                      @RequestParam(value = "update", required = false, defaultValue = "") String isUpdate,
                      HttpServletRequest request) throws IOException {
        if (file != null && file.getContentType().startsWith("image")) {

            String name;
            if (isDefault.equalsIgnoreCase("true")) {
                name = "default.png";

                Image image = new Image();
                image.setName(name);
                image.setLastName(file.getOriginalFilename());

                imageService.save(image);

            } else if (!isUpdate.equals("")) {
                Image image = imageService.get(isUpdate);
                if (image == null) {
                    return "Not Found";
                } else {
                    name = image.getName();
                    image.setLastName(file.getOriginalFilename());
                    imageService.update(image);
                }
            } else {
                while (true) {
                    name = RandomValue.generate(60) + "." + file.getContentType().split("/")[1];
                    if (imageService.get(name) == null) {
                        break;
                    }
                }
                Image image = new Image();
                image.setName(name);
                image.setLastName(file.getOriginalFilename());

                imageService.save(image);
            }

            File file1 = new File(ProjectDetails.IMAGE_LOCATION);
            if (!file1.exists())
                file1.mkdirs();

            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(file1.getAbsolutePath() + File.separator + name));
            bufferedOutputStream.write(file.getBytes());
            bufferedOutputStream.close();
            return name;
        }
        return "Failed";
    }

}
