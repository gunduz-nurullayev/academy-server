package org.nazarov.software.server.spring.api.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.nazarov.software.server.hibernate.entity.Subscriber;
import org.nazarov.software.server.spring.util.ModelBuilderImpl;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by Shahin on 11/3/2016.
 */
public class SubscriberModel extends ModelBuilderImpl<SubscriberModel,Subscriber> {
    private int id;
    @NotNull
    private String mail;
    private String unFollowToken;
    @NotNull
    private boolean enabled;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'" ,locale = "US")
    @NotNull
    private Date created = new Date();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getUnFollowToken() {
        return unFollowToken;
    }

    public void setUnFollowToken(String unFollowToken) {
        this.unFollowToken = unFollowToken;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Override
    public SubscriberModel convertToModel(Subscriber entity) {
        SubscriberModel model = new SubscriberModel();
        model.setId(entity.getId());
        model.setCreated(entity.getCreated());
        model.setMail(entity.getMail());
        model.setUnFollowToken(entity.getUnFollowToken());
        return model;
    }

    @Override
    public Subscriber convertToEntity(Subscriber entity, SubscriberModel model) {
        entity.setCreated(model.getCreated());
        entity.setMail(model.getMail());
        entity.setUnFollowToken(model.getUnFollowToken());
        return entity;
    }

}
