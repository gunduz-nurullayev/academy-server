package org.nazarov.software.server.spring.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

/**
 * Created by Shahin on 3/21/2016.
 */
@Configuration
@PropertySource("classpath:META-INF/configuration/mail.properties")
public class MailConfiguration {

    @Autowired
    private Environment environment;

    @Bean(name = "javamail")
    protected JavaMailSenderImpl mailSender() {
        JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
        javaMailSender.setHost(environment.getProperty("mail.smtp.host"));
        javaMailSender.setPort(Integer.parseInt(environment.getProperty("mail.smtp.port")));
        javaMailSender.setUsername(environment.getProperty("mail.user"));
        javaMailSender.setPassword(environment.getProperty("mail.password"));
        javaMailSender.setDefaultEncoding("UTF-8");
        Properties properties = new Properties();
        properties.setProperty("mail.smtp.auth", environment.getProperty("mail.smtp.auth"));
        properties.setProperty("mail.debug", environment.getProperty("mail.debug"));
        properties.setProperty("mail.transport.protocol", environment.getProperty("mail.transport.protocol"));
        properties.setProperty("mail.smtp.starttls.enable", environment.getProperty("mail.smtp.starttls.enable"));
        javaMailSender.setJavaMailProperties(properties);
        return javaMailSender;
    }

    @Bean(name = "pop3props")
    protected Properties properties() {
        Properties props = System.getProperties();
        props.setProperty("mail.pop3.socketFactory.class", environment.getProperty("mail.pop3.socketFactory.class"));
        props.setProperty("mail.pop3.socketFactory.fallback", environment.getProperty("mail.pop3.socketFactory.fallback"));
        props.setProperty("mail.pop3.port", environment.getProperty("mail.pop3.port"));
        props.setProperty("mail.pop3.socketFactory.port", environment.getProperty("mail.pop3.socketFactory.port"));
        props.put("mail.pop3.host", environment.getProperty("mail.pop3.host"));
        props.put("username", environment.getProperty("mail.user"));
        props.put("password", environment.getProperty("mail.password"));
        return props;
    }


}
