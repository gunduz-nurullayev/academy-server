package org.nazarov.software.server.spring.api.model;

import org.nazarov.software.server.hibernate.entity.Student;
import org.nazarov.software.server.spring.util.ModelBuilderImpl;

import javax.validation.constraints.NotNull;

/**
 * Created by Shahin on 11/3/2016.
 */
public class StudentModel extends ModelBuilderImpl<StudentModel , Student> {

    private long id;
    private String university;
    @NotNull
    private float disCount ;
    private boolean enabled;

    @Override
    public StudentModel convertToModel(Student entity) {
        StudentModel studentModel = new StudentModel();
        studentModel.setId(entity.getId());
        studentModel.setUniversity(entity.getUniversity());
        studentModel.setEnabled(entity.isEnabled());
        studentModel.setDisCount(entity.getDisCount());
        return studentModel;
    }

    @Override
    public Student convertToEntity(Student entity, StudentModel model) {
        entity.setUniversity(model.getUniversity());
        entity.setEnabled(model.isEnabled());
        entity.setDisCount(model.getDisCount());
        return entity;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public float getDisCount() {
        return disCount;
    }

    public void setDisCount(float disCount) {
        this.disCount = disCount;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
