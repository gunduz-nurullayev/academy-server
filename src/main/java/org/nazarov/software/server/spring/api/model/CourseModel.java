package org.nazarov.software.server.spring.api.model;

import org.nazarov.software.server.hibernate.entity.Course;
import org.nazarov.software.server.hibernate.entity.Image;
import org.nazarov.software.server.hibernate.entity.Teacher;
import org.nazarov.software.server.hibernate.service.ImageService;
import org.nazarov.software.server.hibernate.service.TeacherService;
import org.nazarov.software.server.spring.util.ModelBuilderImpl;

import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Shahin on 12/16/2016.
 */
public class CourseModel extends ModelBuilderImpl<CourseModel, Course> {

    private TeacherService teacherService;
    private ImageService imageService;


    @NotNull
    private String name;
    @NotNull
    private double amount;
    private String details;
    private String image;
    @NotNull
    private boolean enabled;
    private Set<Long> teachers;

    public CourseModel(TeacherService teacherService, ImageService imageService) {
        this.teacherService = teacherService;
        this.imageService = imageService;
    }

    public CourseModel() {
    }

    public Set<Long> getTeachers() {
        return teachers;
    }

    public void setTeachers(Set<Long> teachers) {
        this.teachers = teachers;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public CourseModel convertToModel(Course course) {
        if (course == null)
            return null;
        CourseModel model = new CourseModel();
        model.setAmount(course.getAmount());
        model.setName(course.getName());
        model.setDetails(course.getDetails());
        model.setEnabled(course.isEnabled());
        Set<Long> teachers = new HashSet<Long>();
        if (course.getTeachers() != null)
            for (Teacher teacher : course.getTeachers()) {
                teachers.add(teacher.getId());
            }
        model.setTeachers(teachers);

        if (course.getImage() != null)
            model.setImage(course.getImage().getName());
        else
            model.setImage("default.png");
        return model;
    }

    public Course convertToEntity(Course course, CourseModel model) {
        course.setAmount(model.getAmount());
        course.setDetails(model.getDetails());
        course.setEnabled(model.isEnabled());
        if (model.getTeachers() != null) {
            Set<Teacher> teachers = new HashSet<Teacher>();
            for (long id : model.getTeachers()) {
                Teacher teacher = teacherService.get(id);
                if (teacher != null) {
                    teachers.add(teacher);
                }
            }
            course.setTeachers(teachers);
        }
        Image image = imageService.get(model.getImage());
        if (image != null)
            course.setImage(image);
        return course;
    }

}
