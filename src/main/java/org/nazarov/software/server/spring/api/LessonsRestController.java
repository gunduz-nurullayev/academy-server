package org.nazarov.software.server.spring.api;

import org.nazarov.software.server.hibernate.entity.Lessons;
import org.nazarov.software.server.hibernate.service.CourseService;
import org.nazarov.software.server.hibernate.service.GroupService;
import org.nazarov.software.server.hibernate.service.LessonsService;
import org.nazarov.software.server.spring.api.model.LessonModel;
import org.nazarov.software.server.spring.api.model.SearchFilterModel;
import org.nazarov.software.server.spring.util.ViewBuilderImpl;
import org.nazarov.software.server.tool.Server;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Shahin on 11/7/2016.
 */
@RestController
@RequestMapping(Server.API + Server.LESSON)
public class LessonsRestController  extends ViewBuilderImpl<LessonModel, Long> {

    public LessonsRestController(@Autowired CourseService courseService
            , @Autowired LessonsService service) {
        super(new LessonModel(courseService), service);
    }

    @Override
    public ResponseEntity<List<LessonModel>> list() {
        return super.list();
    }

    @Override
    public ResponseEntity<LessonModel> get(@PathVariable("id") Long key) {
        return super.get(key);
    }

    @Override
    public ResponseEntity<Void> create(@RequestBody @Valid LessonModel model, UriComponentsBuilder ucBuilder) {
        return super.create(new Lessons(), model ,Server.LESSON, ucBuilder, model.getId());
    }


    @Override
    public ResponseEntity<LessonModel> update(@PathVariable("id") Long id,@RequestBody @Valid LessonModel model) {
        return super.update(id, model);
    }

    @Override
    public ResponseEntity<LessonModel> delete(@PathVariable("id") Long id) {
        return super.delete(id);
    }

    @Override
    public ResponseEntity<List<LessonModel>> search(@RequestBody @Valid SearchFilterModel searchModel) {
        return super.search(searchModel);
    }
}
