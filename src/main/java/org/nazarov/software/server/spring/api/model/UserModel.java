package org.nazarov.software.server.spring.api.model;

import org.nazarov.software.server.hibernate.entity.User;
import org.nazarov.software.server.hibernate.tool.enums.UserRole;
import org.nazarov.software.server.spring.util.ModelBuilderImpl;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * Created by Shahin on 11/3/2016.
 */
public class UserModel extends ModelBuilderImpl<UserModel ,User> {


    private PasswordEncoder encoder;

    private long id;
    @NotNull
    private String mail;
    @NotNull
    private String password;
    @NotNull
    private boolean enabled;
    private UserRole role;


    public UserModel(PasswordEncoder encoder) {
        this.encoder = encoder;
    }

    public UserModel() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }


    public UserModel convertToModel(User user) {
        if (user == null)
            return null;
        UserModel model = new UserModel();
        model.setMail(user.getMail());
        model.setEnabled(user.isEnabled());
        model.setId(user.getId());
        model.setPassword("**secret**");
        model.setRole(user.getRole());
        return model;
    }

    public User convertToEntity(User currentUser, UserModel user) {
        currentUser.setMail(user.getMail());
        currentUser.setEnabled(user.isEnabled());
        currentUser.setRole(user.getRole());
        currentUser.setPassword(encoder.encode(user.getPassword()));
        return currentUser;
    }

}
