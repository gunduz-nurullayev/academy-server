package org.nazarov.software.server.spring.api;

import org.nazarov.software.server.hibernate.entity.Property;
import org.nazarov.software.server.hibernate.service.PropertyService;
import org.nazarov.software.server.spring.api.model.PropertyMapModel;
import org.nazarov.software.server.spring.api.model.SearchFilterModel;
import org.nazarov.software.server.spring.util.ViewBuilderImpl;
import org.nazarov.software.server.tool.Server;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Shahin on 1/4/2017.
 */
@RestController
@RequestMapping(Server.API + Server.PROPERTYMAP)
public class PropertMapRestController extends ViewBuilderImpl<PropertyMapModel , String>{

    public PropertMapRestController(@Autowired PropertyService service) {
        super(new PropertyMapModel(), service);
    }

    @Override
    public ResponseEntity<Void> create(@RequestBody @Valid PropertyMapModel model, UriComponentsBuilder ucBuilder) {
        Property property = new Property();
        property.setKey(model.getKey());
        return super.create(property, model ,Server.PROPERTYMAP, ucBuilder, model.getKey());
    }

    @Override
    public ResponseEntity<List<PropertyMapModel>> list() {
        return super.list();
    }

    @Override
    public ResponseEntity<PropertyMapModel> get(@PathVariable("id") String key) {
        return super.get(key);
    }

    @Override
    public ResponseEntity<PropertyMapModel> update(@PathVariable("id") String id, @RequestBody @Valid PropertyMapModel model) {
        return super.update(id, model);
    }

    @Override
    public ResponseEntity<PropertyMapModel> delete(@PathVariable("id") String id) {
        return super.delete(id);
    }

    @Override
    public ResponseEntity<List<PropertyMapModel>> search(@RequestBody @Valid SearchFilterModel searchModel) {
        return super.search(searchModel);
    }
}
