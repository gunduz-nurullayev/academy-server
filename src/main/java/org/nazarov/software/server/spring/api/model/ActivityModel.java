package org.nazarov.software.server.spring.api.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.nazarov.software.server.hibernate.entity.Activity;
import org.nazarov.software.server.hibernate.entity.Group;
import org.nazarov.software.server.hibernate.entity.Student;
import org.nazarov.software.server.hibernate.service.GroupService;
import org.nazarov.software.server.hibernate.service.StudentService;
import org.nazarov.software.server.hibernate.tool.enums.ActivityMode;
import org.nazarov.software.server.spring.util.ModelBuilderImpl;

import java.util.Date;
import java.util.Locale;

/**
 * Created by Shahin on 12/17/2016.
 */
public class ActivityModel extends ModelBuilderImpl<ActivityModel, Activity> {

    private long id;
    private String group;
    private long student;
    private ActivityMode mode;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'" ,locale = "US")
    private Date date;

    private GroupService groupService;
    private StudentService studentService;

    public ActivityModel(GroupService groupService, StudentService studentService) {
        this.groupService = groupService;
        this.studentService = studentService;
    }

    public ActivityModel() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public long getStudent() {
        return student;
    }

    public void setStudent(long student) {
        this.student = student;
    }

    public ActivityMode getMode() {
        return mode;
    }

    public void setMode(ActivityMode mode) {
        this.mode = mode;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public ActivityModel convertToModel(Activity activity) {
        if (activity == null)
            return null;
        ActivityModel model = new ActivityModel();
        model.setId(activity.getId());
        model.setDate(activity.getDate());
        if (activity.getGroup() != null) {
            model.setGroup(activity.getGroup().getName());
        }
        model.setMode(activity.getActivityMode());
        if (activity.getStudent() != null) {
            model.setStudent(activity.getStudent().getId());
        }
        return model;
    }

    public Activity convertToEntity(Activity activity, ActivityModel model) {
        activity.setDate(model.getDate());
        Group group = groupService.get(model.getGroup());
        if (group != null) {
            activity.setGroup(group);
        }
        Student student = studentService.get(model.getStudent());
        if (group != null) {
            activity.setStudent(student);
        }
        activity.setActivityMode(model.getMode());
        return activity;
    }

}
