package org.nazarov.software.server.spring.api.model;

import org.nazarov.software.server.hibernate.entity.Teacher;
import org.nazarov.software.server.spring.util.ModelBuilderImpl;

import javax.validation.constraints.NotNull;

/**
 * Created by Shahin on 11/3/2016.
 */
public class TeacherModel extends ModelBuilderImpl<TeacherModel , Teacher>{

    private long id;
    @NotNull
    private double salary;

    @Override
    public TeacherModel convertToModel(Teacher entity) {
        TeacherModel model = new TeacherModel();
        model.setId(entity.getId());
        model.setSalary(entity.getSalary());
        return model;
    }

    @Override
    public Teacher convertToEntity(Teacher entity, TeacherModel model) {
        entity.setSalary(model.getSalary());
        return entity;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }
}
