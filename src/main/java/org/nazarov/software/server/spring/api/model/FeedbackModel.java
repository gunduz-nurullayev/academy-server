package org.nazarov.software.server.spring.api.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.nazarov.software.server.hibernate.entity.Feedback;
import org.nazarov.software.server.spring.util.ModelBuilderImpl;

import java.util.Date;

/**
 * Created by Shahin on 11/3/2016.
 */
public class FeedbackModel extends ModelBuilderImpl<FeedbackModel , Feedback>{

    private int id;
    private String mail;
    private String name;
    private String phone;
    private String message;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'" ,locale = "US")
    private Date created = new Date();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Override
    public FeedbackModel convertToModel(Feedback entity) {
        FeedbackModel model = new FeedbackModel();
        model.setId(entity.getId());
        model.setCreated(entity.getCreated());
        model.setMail(entity.getMail());
        model.setName(entity.getName());
        model.setMessage(entity.getMessage());
        model.setPhone(entity.getPhone());
        return model;
    }

    @Override
    public Feedback convertToEntity(Feedback entity, FeedbackModel model) {
        entity.setCreated(model.getCreated());
        entity.setMail(model.getMail());
        entity.setName(model.getName());
        entity.setMessage(model.getMessage());
        entity.setPhone(model.getPhone());
        return entity;
    }
}
