package org.nazarov.software.server.spring.security;

import org.nazarov.software.server.hibernate.entity.User;
import org.nazarov.software.server.hibernate.service.UserService;
import org.nazarov.software.server.hibernate.tool.enums.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Shahin on 10/10/2016.
 */
@Service("userDetailsService")
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {


    //get user from the database, via Hibernate
    @Autowired
    private UserService userService;

    public UserDetails loadUserByUsername(String mail)
            throws UsernameNotFoundException {
        User user = userService.findByMail(mail);
        if (user == null) {
            System.out.println("UserModel not found");
            throw new UsernameNotFoundException("Username not found");
        }
        org.nazarov.software.server.hibernate.entity.UserDetails userDetails = user.getUserDetails();
        if (userDetails != null) {
            userDetails.setLastAccess(new Date());
            user.setUserDetails(userDetails);
            userService.update(user);
        }
        return new org.springframework.security.core.userdetails.User(user.getMail(), user.getPassword(),
                user.isEnabled(), true, true, true, getGrantedAuthorities(user));
    }


    private List<GrantedAuthority> getGrantedAuthorities(User user) {
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        for (int i = 0; i <= user.getRole().ordinal(); i++) {
            authorities.add(new SimpleGrantedAuthority("ROLE_" + UserRole.values()[i]));
        }
        return authorities;
    }

}
