package org.nazarov.software.server.spring.api.model;

import org.nazarov.software.server.hibernate.entity.Image;
import org.nazarov.software.server.hibernate.tool.enums.ImageFolder;
import org.nazarov.software.server.spring.util.ModelBuilderImpl;

import javax.validation.constraints.NotNull;

/**
 * Created by Shahin on 3/20/2017.
 */
public class ImageModel extends ModelBuilderImpl<ImageModel,Image>{

    private String name;
    @NotNull
    private String lastName;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public ImageModel convertToModel(Image entity) {
        ImageModel model = new ImageModel();
        model.setName(entity.getName());
        model.setLastName(entity.getLastName());
        return model;
    }

    @Override
    public Image convertToEntity(Image entity, ImageModel model) {
        entity.setLastName(model.getLastName());
        return entity;
    }
}
