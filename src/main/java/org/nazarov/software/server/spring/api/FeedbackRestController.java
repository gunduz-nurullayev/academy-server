package org.nazarov.software.server.spring.api;

import org.nazarov.software.server.hibernate.entity.Feedback;
import org.nazarov.software.server.hibernate.service.FeedbackService;
import org.nazarov.software.server.spring.api.model.FeedbackModel;
import org.nazarov.software.server.spring.api.model.SearchFilterModel;
import org.nazarov.software.server.spring.util.ViewBuilderImpl;
import org.nazarov.software.server.tool.Server;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Shahin on 1/4/2017.
 */
@RestController
@RequestMapping(Server.API + Server.FEEDBACK)
public class FeedbackRestController extends ViewBuilderImpl<FeedbackModel , Integer> {

    public FeedbackRestController(@Autowired FeedbackService service) {
        super(new FeedbackModel(), service);
    }

    @Override
    public ResponseEntity<Void> create(@RequestBody @Valid FeedbackModel model, UriComponentsBuilder ucBuilder) {
        return super.create(new Feedback() , model ,Server.FEEDBACK, ucBuilder, model.getId());
    }

    @Override
    public ResponseEntity<List<FeedbackModel>> list() {
        return super.list();
    }

    @Override
    public ResponseEntity<FeedbackModel> get(@PathVariable("id") Integer key) {
        return super.get(key);
    }


    @Override
    public ResponseEntity<FeedbackModel> update(@PathVariable("id") Integer id,@RequestBody @Valid FeedbackModel model) {
        return super.update(id, model);
    }

    @Override
    public ResponseEntity<FeedbackModel> delete(@PathVariable("id") Integer id) {
        return super.delete(id);
    }

    @Override
    public ResponseEntity<List<FeedbackModel>> search(@RequestBody @Valid SearchFilterModel searchModel) {
        return super.search(searchModel);
    }
}
