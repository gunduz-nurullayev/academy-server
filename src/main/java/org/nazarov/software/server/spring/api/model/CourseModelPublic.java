package org.nazarov.software.server.spring.api.model;

import org.nazarov.software.server.hibernate.entity.Course;
import org.nazarov.software.server.hibernate.entity.Teacher;
import org.nazarov.software.server.hibernate.entity.User;
import org.nazarov.software.server.hibernate.service.UserService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Shahin on 3/22/2017.
 */
public class CourseModelPublic {

    public CourseModelPublic() {
    }

    private String name;
    private double price;
    private String details;
    private String image;
    private List<UserDetailsModelPublic> teachers;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<UserDetailsModelPublic> getTeachers() {
        return teachers;
    }

    public void setTeachers(List<UserDetailsModelPublic> teachers) {
        this.teachers = teachers;
    }

    private CourseModelPublic convertToModel(Course course) {
        if (course == null)
            return null;
        CourseModelPublic model = new CourseModelPublic();
        model.setPrice(course.getAmount());
        model.setName(course.getName());
        model.setDetails(course.getDetails());

        List<UserDetailsModelPublic> userDetailsModel = new ArrayList();
        if (course.getTeachers() != null)
            for (Teacher teacher : course.getTeachers()) {
                User user = teacher.getUser();
                if (user != null && user.getUserDetails() != null) {
                    userDetailsModel.add(UserDetailsModelPublic.convertToModel(user.getUserDetails()));
                }
            }
        model.setTeachers(userDetailsModel);

        if (course.getImage() != null)
            model.setImage(course.getImage().getName());
        else
            model.setImage("default.png");
        return model;
    }

    public List<CourseModelPublic> convertToModel(List<Course> courses) {
        List<CourseModelPublic> courseModelPublics = new ArrayList();
        for (Course c : courses)
            courseModelPublics.add(convertToModel(c));
        return courseModelPublics;
    }
}
