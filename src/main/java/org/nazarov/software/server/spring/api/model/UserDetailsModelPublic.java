package org.nazarov.software.server.spring.api.model;

import org.nazarov.software.server.hibernate.entity.UserDetails;
import org.nazarov.software.server.hibernate.tool.enums.Gender;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Shahin on 3/22/2017.
 */
public class UserDetailsModelPublic {

    private String fullName;
    private String details;
    private Gender gender;
    private String imageName;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public static UserDetailsModelPublic convertToModel(UserDetails entity) {
        UserDetailsModelPublic model = new UserDetailsModelPublic();
        model.setFullName(entity.getFullName());
        model.setDetails(entity.getDetails());
        model.setGender(entity.getGender());
        if(entity.getImage() != null){
            model.setImageName(entity.getImage().getName());
        }
        return model;
    }

    public List<UserDetailsModelPublic> convertToModel (List<UserDetails> userDetails){
        List<UserDetailsModelPublic> userDetailsModelPublics = new ArrayList();
        for(UserDetails u : userDetails)
            userDetailsModelPublics.add(convertToModel(u));
        return userDetailsModelPublics;
    }
}
