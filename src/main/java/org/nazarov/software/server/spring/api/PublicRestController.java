package org.nazarov.software.server.spring.api;

import org.nazarov.software.server.hibernate.entity.*;
import org.nazarov.software.server.hibernate.service.*;
import org.nazarov.software.server.hibernate.tool.SearchModel;
import org.nazarov.software.server.hibernate.tool.enums.SearchAttribute;
import org.nazarov.software.server.spring.api.model.*;
import org.nazarov.software.server.spring.service.MailSender;
import org.nazarov.software.server.tool.Server;
import org.nazarov.software.server.tool.details.ProjectDetails;
import org.shaheen.nazarov.tools.util.RandomValue;
import org.shaheen.nazarov.tools.util.Regex;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.*;
import java.security.Principal;
import java.util.*;

/**
 * Created by Shahin on 12/22/2016.
 */
@RestController
@RequestMapping(Server.PUBLIC)
public class PublicRestController {

    @Autowired
    private UserService userService;

    @Autowired
    private FeedbackService feedbackService;

    @Autowired
    private SubscriberService subscriberService;

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private ImageService imageService;

    @Autowired
    private CourseService courseService;

    @Autowired
    private TeacherService teacherService;

    @Autowired
    private MailSender mailSender;

    @RequestMapping(path = Server.PUBLIC_FORGOT, method = RequestMethod.POST)
    public ResponseEntity<Void> sendEmail(@RequestParam("email") String email) {
        if (email.matches(Regex.EMAIL_REGEX)) {
            User user = userService.findByMail(email);
            if (user != null) {
                Subscriber subscriber = new Subscriber();
                subscriber.setMail(email);
                Map<String, String> map = new HashMap();
                map.put("_recovery_url_", ProjectDetails.PROJECT_URL + Server.PUBLIC + "/recover?email=" + email + "&token=" + user.getUserDetails().getToken());
                mailSender.sendMail("forgotPassword", map, subscriber);
                return new ResponseEntity<Void>(HttpStatus.OK);
            }
        }
        return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
    }


    @RequestMapping(path = Server.PUBLIC_RECOVER, method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    public void recover(HttpServletResponse response, @RequestParam("email") String email
            , @RequestParam("token") String token) {
        response.setContentType("text/html");
        try {
            PrintWriter out = response.getWriter();
            if (email.matches(Regex.EMAIL_REGEX)) {
                User user = userService.findByMail(email);
                if (user != null && token.equals(user.getUserDetails().getToken())) {
                    out.println("<HTML><HEAD><TITLE>Recover Password!</TITLE>\n" +
                            "</HEAD><BODY><h1>Enter your new Password</h1>" +
                            "<form name=\"login\" action=\"/public/recover?email=" + email + "&token=" + token + "\" method=\"post\" accept-charset=\"utf-8\">\n" +
                            "  <ul>\n" +
                            "    <li><label for=\"password\">Password</label>\n" +
                            "    <input type=\"password\" name=\"password\" placeholder=\"password\" required></li>\n" +
                            "    <li><label for=\"password\">Password Again</label>\n" +
                            "    <input type=\"password\" name=\"password_again\" placeholder=\"password\" required></li>\n" +
                            "    <li>\n" +
                            "    <input type=\"submit\" value=\"Update\"></li>\n" +
                            "  </ul>\n" +
                            "</form>\n" +
                            "</BODY></HTML>");
                    out.close();
                    return;
                }
            }

            out.println("Your email or token is not valid.");
            out.close();
        } catch (IOException ex) {

        }
    }

    @RequestMapping(path = Server.PUBLIC_RECOVER, method = RequestMethod.POST, produces = MediaType.TEXT_HTML_VALUE)
    public void recover(HttpServletResponse response, @RequestParam("email") String email, @RequestParam("token") String token
            , @RequestParam("password") String password, @RequestParam("password_again") String passwordAgain) {

        response.setContentType("text/html");
        try {
            PrintWriter out = response.getWriter();
            if (email.matches(Regex.EMAIL_REGEX)) {
                User user = userService.findByMail(email);
                if (user != null && token.equals(user.getUserDetails().getToken())) {
                    if (password.matches(Regex.PASSWORD_LIGHT_REGEX) && password.equals(passwordAgain)) {
                        user.setPassword(encoder.encode(password));
                        userService.update(user);
                        out.println("Your password successfully updated !");
                        out.close();
                        return;
                    }
                    out.println("<HTML><HEAD><TITLE>Recover Password!</TITLE>\n" +
                            "</HEAD><BODY><h1>" + Regex.PASSWORD_LIGHT_MESSAGE + "</h1>" +
                            "<form name=\"login\" action=\"/public/recover?email=" + email + "&token=" + token + "\" method=\"post\" accept-charset=\"utf-8\">\n" +
                            "<ul>\n" +
                            "    <li><label for=\"password\">Password</label>\n" +
                            "    <input type=\"password\" name=\"password\" placeholder=\"password\" required></li>\n" +
                            "    <li><label for=\"password\">Password Again</label>\n" +
                            "    <input type=\"password\" name=\"password_again\" placeholder=\"password\" required></li>\n" +
                            "    <li>\n" +
                            "    <input type=\"submit\" value=\"Update\"></li>\n" +
                            "  </ul>\n" +
                            "</form>\n" +
                            "</BODY></HTML>");
                    out.close();
                    return;
                }
            }
            out.println("Your email or token is not valid.");
            out.close();
        } catch (IOException ex) {

        }
    }

    @RequestMapping(path = Server.PUBLIC_FEEDBACK, method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE}
            , produces = MediaType.TEXT_PLAIN_VALUE)
    public String sendFeedback(@RequestBody @Valid FeedbackModel model) {
        Feedback feedback = new Feedback();
        feedback = model.convertToEntity(feedback, model);
        feedbackService.save(feedback);
        return "Successfully send.";
    }

    @RequestMapping(path = Server.PUBLIC_SUBSCRIBE, method = RequestMethod.POST, produces = MediaType.TEXT_PLAIN_VALUE)
    public String subscribe(@RequestParam("email") String email) {
        if (email.matches(Regex.EMAIL_REGEX)) {
            Subscriber subscriber = subscriberService.checkEmail(email);
            if (subscriber == null) {
                subscriber = new Subscriber();
                subscriber.setMail(email);
                subscriber.setCreated(new Date());
                subscriber.setEnabled(true);
                subscriber.setUnFollowToken(RandomValue.generate(30));
                subscriberService.save(subscriber);
                return "Your email successfully subscribed.";
            }
            return "Your email already subscribed.";
        }
        return "Your email is invalid.";
    }

    @RequestMapping(path = Server.PUBLIC_UNSUBSCRIBE, method = RequestMethod.POST, produces = MediaType.TEXT_PLAIN_VALUE)
    public String unSubscribe(@RequestParam("email") String email, @RequestParam("token") String token) {
        if (email.matches(Regex.EMAIL_REGEX)) {
            Subscriber subscriber = subscriberService.checkUnsubscribe(email, token);
            if (subscriber != null) {
                subscriber.setEnabled(false);
                subscriber.setUnFollowToken(RandomValue.generate(30));
                subscriberService.update(subscriber);
                return "Your email successfully unsubscribed.";
            }
            return "Your email or token is not correct.";
        }
        return "Your email is invalid.";
    }

    @RequestMapping(value = Server.PUBLIC_DOWNLOAD ,method = RequestMethod.GET)
    public HttpServletResponse response(@RequestParam("name") String id , HttpServletRequest request , HttpServletResponse response) throws ServletException, IOException {

        if(imageService.get(id) != null){

            File file = new File(ProjectDetails.IMAGE_LOCATION
                    +File.separator + id);

            if(file.exists()){

                FileInputStream fileInputStream = new FileInputStream(file);
                String mimeType = request.getServletContext().getMimeType(file.getAbsolutePath());
                if(mimeType == null){
                    mimeType = "application/octet-stream";
                }
                response.setContentType(mimeType);
                response.setContentLength((int) file.length());

                String headerKey = "Content-Disposition";
                String headerValue = String.format("attachment: filename=\"%s\"", file.getName());

                response.setHeader(headerKey , headerValue);

                OutputStream outputStream = response.getOutputStream();

                byte[] buffer = new byte[4096];
                int bytesRead = -1;
                while ((bytesRead = fileInputStream.read(buffer)) != -1){
                    outputStream.write(buffer,0,bytesRead);
                }
                response.setStatus(200);
                fileInputStream.close();
                outputStream.close();
            }
        }
        response.setStatus(404);
        response.getWriter().write("Not Found");
        response.getWriter().close();
        return response;
    }

    @RequestMapping(path = Server.PUBLIC_COURSES, method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<List<CourseModelPublic>> courseModels(@RequestParam(value = "name",required = false) String name) {
        CourseModelPublic courseModel = new CourseModelPublic();
       if(name == null || name.isEmpty()){
           SearchModel searchModel = new SearchModel("enabled" , SearchAttribute.EQUALS,false,true);
           List<Course> courses = courseService.advanceSearch(searchModel);
           if(courses == null || courses.size() == 0){
               return new ResponseEntity<List<CourseModelPublic>>(HttpStatus.NOT_FOUND);
           }
           return ResponseEntity.ok(courseModel.convertToModel(courses));
       }
       else{
           SearchModel searchModel = new SearchModel("enabled" , SearchAttribute.EQUALS,true,true);
           SearchModel searchModel2 = new SearchModel("name" , SearchAttribute.EQUALS,false,name);
           List<Course> courses = courseService.advanceSearch(searchModel,searchModel2);
           if(courses == null || courses.size() == 0){
               return new ResponseEntity<List<CourseModelPublic>>(HttpStatus.NOT_FOUND);
           }
           return ResponseEntity.ok(courseModel.convertToModel(courses));
       }
    }

    @RequestMapping(path = Server.PUBLIC_TEACHERS, method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<List<UserDetailsModelPublic>> teacherModels(@RequestParam(value = "name",required = false) String name) {
        List<UserDetails> userDetails;
        if(name == null || name.isEmpty())
            userDetails = teacherService.getUserDetails();
        else
            userDetails = teacherService.getUserDetails(name);
        if(userDetails == null || userDetails.size() == 0){
            return new ResponseEntity<List<UserDetailsModelPublic>>(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(new UserDetailsModelPublic().convertToModel(userDetails));
    }

}
