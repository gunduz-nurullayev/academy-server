package org.nazarov.software.server.spring.util;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Shahin on 12/17/2016.
 */
public interface ModelBuilder<M,E> extends Serializable {

    M convertToModel(E entity);
    List<M> convertToModel(List<E> entities);
    E convertToEntity(E entity, M model);

}
