package org.nazarov.software.server.spring.api;

import org.nazarov.software.server.hibernate.entity.Student;
import org.nazarov.software.server.hibernate.service.StudentService;
import org.nazarov.software.server.spring.api.model.SearchFilterModel;
import org.nazarov.software.server.spring.api.model.StudentModel;
import org.nazarov.software.server.spring.util.ViewBuilderImpl;
import org.nazarov.software.server.tool.Server;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Shahin on 11/7/2016.
 */
@RestController
@RequestMapping(Server.API + Server.STUDENT)
public class StudentRestController extends ViewBuilderImpl<StudentModel , Long> {

    public StudentRestController(@Autowired StudentService service) {
        super(new StudentModel(), service);
    }

    @Override
    public ResponseEntity<List<StudentModel>> list() {
        return super.list();
    }

    @Override
    public ResponseEntity<Void> create(@RequestBody @Valid StudentModel model, UriComponentsBuilder ucBuilder) {
        return super.create(new Student(), model,  Server.STUDENT, ucBuilder, model.getId());
    }

    @Override
    public ResponseEntity<StudentModel> get(@PathVariable("id") Long key) {
        return super.get(key);
    }

    @Override
    public ResponseEntity<StudentModel> update(@PathVariable("id") Long id, @RequestBody @Valid StudentModel model) {
        return super.update(id, model);
    }

    @Override
    public ResponseEntity<StudentModel> delete(@PathVariable("id") Long id) {
        return super.delete(id);
    }

    @Override
    public ResponseEntity<List<StudentModel>> search(@RequestBody @Valid SearchFilterModel searchModel) {
        return super.search(searchModel);
    }
}
