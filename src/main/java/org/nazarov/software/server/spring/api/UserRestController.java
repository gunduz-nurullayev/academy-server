package org.nazarov.software.server.spring.api;

import org.nazarov.software.server.hibernate.entity.User;
import org.nazarov.software.server.hibernate.service.UserService;
import org.nazarov.software.server.spring.api.model.SearchFilterModel;
import org.nazarov.software.server.spring.api.model.UserModel;
import org.nazarov.software.server.spring.util.ViewBuilderImpl;
import org.nazarov.software.server.tool.Server;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

/**
 * Created by Shahin on 11/3/2016.
 */
@RestController
@RequestMapping(Server.API + Server.USER)
public class UserRestController extends ViewBuilderImpl<UserModel, Long> {

    @Autowired
    private UserService userService;


    public UserRestController(@Autowired UserService userService,
                              @Autowired PasswordEncoder encoder) {
        super(new UserModel(encoder), userService);
    }


    public ResponseEntity<List<UserModel>> list() {
        return super.list();
    }


    @Secured("hasRole('ADMIN')")
    public ResponseEntity<UserModel> get(@PathVariable("id") Long id) {
        return super.get(id);
    }


    @RequestMapping(value ="/info", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<UserModel> info(Principal principal) {
        User user = userService.findByMail(principal.getName());
        if(user == null){
            return null;
        }
        return super.get(user.getId());
    }


    public ResponseEntity<Void> create(@RequestBody @Valid UserModel model, UriComponentsBuilder ucBuilder) {
        User user = userService.findByMail(model.getMail());
        if(user != null){
            return conflict();
        }
        return super.create(new User(), model, Server.USER, ucBuilder, model.getId());
    }

    public ResponseEntity<UserModel> update(@PathVariable("id") Long id, @RequestBody @Valid UserModel model) {
        return super.update(id, model);
    }

    public ResponseEntity<UserModel> delete(@PathVariable("id") Long id) {
        return super.delete(id);
    }

    public ResponseEntity<List<UserModel>> search(
            @RequestBody @Valid  SearchFilterModel searchModel
    ) {
        return super.search(searchModel);
    }

}
