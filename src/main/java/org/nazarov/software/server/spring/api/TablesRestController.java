package org.nazarov.software.server.spring.api;

import org.nazarov.software.server.hibernate.entity.Tables;
import org.nazarov.software.server.hibernate.service.GroupService;
import org.nazarov.software.server.hibernate.service.TablesService;
import org.nazarov.software.server.spring.api.model.SearchFilterModel;
import org.nazarov.software.server.spring.api.model.TableModel;
import org.nazarov.software.server.spring.util.ViewBuilderImpl;
import org.nazarov.software.server.tool.Server;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Shahin on 11/7/2016.
 */
@RestController
@RequestMapping(Server.API + Server.TABLE)
public class TablesRestController extends ViewBuilderImpl<TableModel , Long> {


    public TablesRestController(@Autowired TablesService service,
                                @Autowired GroupService groupService) {
        super(new TableModel(groupService), service);
    }

    @Override
    public ResponseEntity<List<TableModel>> list() {
        return super.list();
    }

    @Override
    public ResponseEntity<TableModel> get(@PathVariable("id") Long key) {
        return super.get(key);
    }

    @Override
    public ResponseEntity<Void> create(@RequestBody @Valid TableModel model, UriComponentsBuilder ucBuilder) {
        return super.create(new Tables(), model, Server.TABLE, ucBuilder, model.getId());
    }

    @Override
    public ResponseEntity<TableModel> update(@PathVariable("id") Long id, @RequestBody @Valid TableModel model) {
        return super.update(id, model);
    }

    @Override
    public ResponseEntity<TableModel> delete(@PathVariable("id") Long id) {
        return super.delete(id);
    }

    @Override
    public ResponseEntity<List<TableModel>> search(@RequestBody @Valid SearchFilterModel searchModel) {
        return super.search(searchModel);
    }
}
