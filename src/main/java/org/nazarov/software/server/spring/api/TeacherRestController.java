package org.nazarov.software.server.spring.api;

import org.nazarov.software.server.hibernate.entity.Teacher;
import org.nazarov.software.server.hibernate.service.TeacherService;
import org.nazarov.software.server.spring.api.model.SearchFilterModel;
import org.nazarov.software.server.spring.api.model.TeacherModel;
import org.nazarov.software.server.spring.util.ViewBuilderImpl;
import org.nazarov.software.server.tool.Server;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Shahin on 11/7/2016.
 */
@RestController
@RequestMapping(Server.API + Server.TEACHER)
public class TeacherRestController extends ViewBuilderImpl<TeacherModel , Long> {


    public TeacherRestController(@Autowired TeacherService service) {
        super(new TeacherModel(), service);
    }


    @Override
    public ResponseEntity<Void> create(@RequestBody @Valid TeacherModel model, UriComponentsBuilder ucBuilder) {
        return super.create(new Teacher(), model ,Server.TEACHER, ucBuilder, model.getId());

    }

    @Override
    public ResponseEntity<List<TeacherModel>> list() {
        return super.list();
    }

    @Override
    public ResponseEntity<TeacherModel> get(@PathVariable("id") Long key) {
        return super.get(key);
    }

    @Override
    public ResponseEntity<TeacherModel> update(@PathVariable("id") Long id, @RequestBody @Valid TeacherModel model) {
        return super.update(id, model);
    }

    @Override
    public ResponseEntity<TeacherModel> delete(@PathVariable("id") Long id) {
        return super.delete(id);
    }

    @Override
    public ResponseEntity<List<TeacherModel>> search(@RequestBody @Valid SearchFilterModel searchModel) {
        return super.search(searchModel);
    }
}
