package org.nazarov.software.server.spring.service;

import org.nazarov.software.server.hibernate.entity.MailTemplate;
import org.nazarov.software.server.hibernate.entity.Subscriber;
import org.nazarov.software.server.hibernate.service.MailTemplateService;
import org.nazarov.software.server.tool.details.ProjectDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Map;

/**
 * Created by Shahin on 10/17/2016.
 */
@Component
public class MailSender {

    @Autowired
    private MailTemplateService mailTemplateService;
    @Autowired
    private JavaMailSender mailSender;

    public boolean sendMail(String name, Map<String, String> params, Subscriber... subscribers) {
        final MailTemplate mailTemplate = mailTemplateService.get(name);
        if (mailTemplate == null) {
            return false;
        }

        String subject = mailTemplate.getSubject()
                .replaceAll("_site_Name_", ProjectDetails.PROJECT_NAME)
                .replaceAll("_site_Address_", ProjectDetails.PROJECT_NAME);
        String text = mailTemplate.getHtmlText()
                .replaceAll("_site_Name_", ProjectDetails.PROJECT_NAME)
                .replaceAll("_site_Address_", ProjectDetails.PROJECT_NAME);
        if (params != null)
            for (String key : params.keySet()) {
                subject = subject.replaceAll(key, params.get(key));
                text = text.replaceAll(key, params.get(key));
            }

        final String finalSubject = subject;
        final String finalText = text;

        for (final Subscriber subscriber : subscribers) {
            mailSender.send(new MimeMessagePreparator() {
                public void prepare(MimeMessage mimeMessage) throws MessagingException {
                    MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, "UTF-8");
                    message.setTo(subscriber.getMail());
                    message.setSubject(finalSubject);
                    message.setText(finalText, mailTemplate.isHtmlMode());
                }
            });
        }
        return true;
    }

    public boolean sendMail(String name, final String email) {
        final MailTemplate mailTemplate = mailTemplateService.get(name);
        if (mailTemplate == null) {
            return false;
        }

        String subject = mailTemplate.getSubject()
                .replaceAll("_site_Name_", ProjectDetails.PROJECT_NAME)
                .replaceAll("_site_Address_", ProjectDetails.PROJECT_NAME);
        String text = mailTemplate.getHtmlText()
                .replaceAll("_site_Name_", ProjectDetails.PROJECT_NAME)
                .replaceAll("_site_Address_", ProjectDetails.PROJECT_NAME);

        final String finalSubject = subject;
        final String finalText = text;
        System.out.println(text);
        mailSender.send(new MimeMessagePreparator() {
            public void prepare(MimeMessage mimeMessage) throws MessagingException {
                MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, "UTF-8");
                message.setTo(email);
                message.setSubject(finalSubject);
                message.setText(finalText, mailTemplate.isHtmlMode());
            }
        });
        return true;
    }

}
