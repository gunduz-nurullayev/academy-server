package org.nazarov.software.server.spring.api.model;

import org.nazarov.software.server.hibernate.tool.SearchModel;

import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * Created by Shahin on 11/6/2016.
 */
public class SearchFilterModel {

    @NotNull
    private SearchModel[] searchModels;
    @NotNull
    private String[] orders;
    private int firstResult;
    private int maxResult;

    public SearchModel[] getSearchModels() {
        return searchModels;
    }

    public void setSearchModels(SearchModel[] searchModels) {
        this.searchModels = searchModels;
    }

    public String[] getOrders() {
        return orders;
    }

    public void setOrders(String[] orders) {
        this.orders = orders;
    }

    public int getFirstResult() {
        return firstResult;
    }

    public void setFirstResult(int firstResult) {
        this.firstResult = firstResult;
    }

    public int getMaxResult() {
        return maxResult;
    }

    public void setMaxResult(int maxResult) {
        this.maxResult = maxResult;
    }
}
