package org.nazarov.software.server.spring.util;

import org.nazarov.software.server.hibernate.tool.DefaultDao;
import org.nazarov.software.server.spring.api.model.SearchFilterModel;
import org.nazarov.software.server.tool.Server;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Shahin on 12/17/2016.
 */
public abstract class ViewBuilderImpl<M, PK extends Serializable> implements ViewBuilder<M, PK> {

    private ModelBuilder modelBuilder;
    private DefaultDao defaultDao;

    protected ModelBuilder getModelBuilder() {
        return modelBuilder;
    }

    protected DefaultDao getDefaultDao() {
        return defaultDao;
    }

    public ViewBuilderImpl(ModelBuilder builder, DefaultDao dao) {
        this.modelBuilder = builder;
        this.defaultDao = dao;
    }

    public ResponseEntity<List<M>> list() {
        List<M> models = modelBuilder.convertToModel(defaultDao.list());
        if (models == null || models.isEmpty()) {
            return noContent();
        }
        return okList(models);
    }

    public ResponseEntity<M> get(PK key) {
        M model = (M) modelBuilder.convertToModel(defaultDao.get(key));
        if (model == null) {
            notFound();
        }
        return ok(model);
    }


    public ResponseEntity<Void> create(Object object, M model, String path, UriComponentsBuilder ucBuilder, Serializable id) {
        if (defaultDao.get(id) != null) {
            return conflict();
        }
        defaultDao.save(modelBuilder.convertToEntity(object, model));
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path(Server.API + path + "/{id}").buildAndExpand(id).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }


    public ResponseEntity<M> update(PK id, M model) {
        M entity = (M) defaultDao.get(id);
        if (entity == null)
            return notFound();
        entity = (M) modelBuilder.convertToEntity(entity, model);
        defaultDao.update(entity);
        return ok((M) modelBuilder.convertToModel(entity));
    }


    public ResponseEntity<M> delete(PK id) {
        M model = (M) defaultDao.get(id);
        if (model == null)
            return notFound();
        defaultDao.remove(model);
        return noContentModel();
    }

    public ResponseEntity<List<M>> search(SearchFilterModel searchModel) {
        List<M> models = modelBuilder.convertToModel(defaultDao.advanceSearch( searchModel.getOrders()
                , searchModel.getFirstResult(), searchModel.getMaxResult(),searchModel.getSearchModels()));
        if (models == null || models.isEmpty()) {
            return noContent();
        }
        return okList(models);
    }

    @Override
    @RequestMapping(method = RequestMethod.POST,consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public abstract ResponseEntity<Void> create(@RequestBody M model, UriComponentsBuilder ucBuilder) ;

    public ResponseEntity<M> notFound() {
        return new ResponseEntity<M>(HttpStatus.NOT_FOUND);
    }

    public ResponseEntity<Void> conflict() {
        return new ResponseEntity<Void>(HttpStatus.CONFLICT);
    }

    public ResponseEntity<List<M>> noContent() {
        return new ResponseEntity<List<M>>(HttpStatus.NO_CONTENT);
    }

    public ResponseEntity<M> noContentModel() {
        return new ResponseEntity<M>(HttpStatus.NO_CONTENT);
    }

    public ResponseEntity<List<M>> okList(List<M> models) {
        return new ResponseEntity<List<M>>(models, HttpStatus.OK);
    }

    public ResponseEntity<M> ok(M model) {
        return new ResponseEntity<M>(model, HttpStatus.OK);
    }


}
