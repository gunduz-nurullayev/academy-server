package org.nazarov.software.server.spring.api;

import org.nazarov.software.server.hibernate.entity.Payment;
import org.nazarov.software.server.hibernate.service.GroupService;
import org.nazarov.software.server.hibernate.service.PaymentService;
import org.nazarov.software.server.hibernate.service.StudentService;
import org.nazarov.software.server.hibernate.service.UserService;
import org.nazarov.software.server.spring.api.model.PaymentModel;
import org.nazarov.software.server.spring.api.model.SearchFilterModel;
import org.nazarov.software.server.spring.util.ViewBuilderImpl;
import org.nazarov.software.server.tool.Server;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Shahin on 11/7/2016.
 */
@RestController
@RequestMapping(Server.API + Server.PAYMENT)
public class PaymentRestController extends ViewBuilderImpl<PaymentModel, Long> {

    public PaymentRestController(@Autowired PaymentService service,
                                 @Autowired GroupService groupService,
                                 @Autowired StudentService studentService,
                                 @Autowired UserService userService) {
        super(new PaymentModel(groupService,studentService,userService), service);
    }

    @Override
    public ResponseEntity<Void> create(@RequestBody @Valid PaymentModel model, UriComponentsBuilder ucBuilder) {
        return super.create(new Payment(), model, Server.PAYMENT, ucBuilder, model.getId());
    }

    @Override
    public ResponseEntity<List<PaymentModel>> list() {
        return super.list();
    }

    @Override
    public ResponseEntity<PaymentModel> get(@PathVariable("id") Long key) {
        return super.get(key);
    }

    @Override
    public ResponseEntity<PaymentModel> update(@PathVariable("id") Long id,@RequestBody @Valid PaymentModel model) {
        return super.update(id, model);
    }

    @Override
    public ResponseEntity<PaymentModel> delete(@PathVariable("id") Long id) {
        return super.delete(id);
    }

    @Override
    public ResponseEntity<List<PaymentModel>> search(@RequestBody @Valid SearchFilterModel searchModel) {
        return super.search(searchModel);
    }
}
