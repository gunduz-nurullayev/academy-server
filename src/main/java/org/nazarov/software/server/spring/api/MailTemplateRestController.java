package org.nazarov.software.server.spring.api;

import org.nazarov.software.server.hibernate.entity.MailTemplate;
import org.nazarov.software.server.hibernate.service.MailTemplateService;
import org.nazarov.software.server.spring.api.model.MailTemplateModel;
import org.nazarov.software.server.spring.api.model.SearchFilterModel;
import org.nazarov.software.server.spring.util.ViewBuilderImpl;
import org.nazarov.software.server.tool.Server;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Shahin on 12/22/2016.
 */
@RestController
@RequestMapping(Server.API + Server.MAILTEMPLATE)
public class MailTemplateRestController extends ViewBuilderImpl<MailTemplateModel, String> {

    public MailTemplateRestController(@Autowired MailTemplateService service) {
        super(new MailTemplateModel(), service);
    }


    @Override
    public ResponseEntity<Void> create(@RequestBody @Valid MailTemplateModel model, UriComponentsBuilder ucBuilder) {
        MailTemplate mailTemplate = new MailTemplate();
        mailTemplate.setName(model.getName());
        return super.create(mailTemplate, model, Server.PAYMENT, ucBuilder, model.getName());
    }

    @Override
    public ResponseEntity<List<MailTemplateModel>> list() {
        return super.list();
    }

    @Override
    public ResponseEntity<MailTemplateModel> get(@PathVariable("id") String key) {
        return super.get(key);
    }

    @Override
    public ResponseEntity<MailTemplateModel> update(@PathVariable("id") String id, @RequestBody @Valid MailTemplateModel model) {
        return super.update(id, model);
    }

    @Override
    public ResponseEntity<MailTemplateModel> delete(@PathVariable("id") String id) {
        return super.delete(id);
    }

    @Override
    public ResponseEntity<List<MailTemplateModel>> search(@RequestBody @Valid SearchFilterModel searchModel) {
        return super.search(searchModel);
    }
}
