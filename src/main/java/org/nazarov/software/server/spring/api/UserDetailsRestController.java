package org.nazarov.software.server.spring.api;

import org.nazarov.software.server.hibernate.entity.UserDetails;
import org.nazarov.software.server.hibernate.service.UserDetailsService;
import org.nazarov.software.server.spring.api.model.SearchFilterModel;
import org.nazarov.software.server.spring.api.model.UserDetailsModel;
import org.nazarov.software.server.spring.util.ViewBuilderImpl;
import org.nazarov.software.server.tool.Server;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Shahin on 11/7/2016.
 */
@RestController
@RequestMapping(Server.API +  Server.USERDETAIL)
public class UserDetailsRestController extends ViewBuilderImpl<UserDetailsModel , Integer>{

    public UserDetailsRestController(@Autowired UserDetailsService userService) {
        super(new UserDetailsModel(), userService);
    }


    @Override
    public ResponseEntity<List<UserDetailsModel>> list() {
        return super.list();
    }

    @Override
    public ResponseEntity<UserDetailsModel> get(@PathVariable("id") Integer key) {
        return super.get(key);
    }

    @Override
    public ResponseEntity<Void> create(@RequestBody @Valid UserDetailsModel model, UriComponentsBuilder ucBuilder) {
        UserDetails userDetails = new UserDetails();
        userDetails.setId(model.getId());
        return super.create(userDetails, model,  Server.USERDETAIL, ucBuilder, model.getId());
    }

    @Override
    public ResponseEntity<UserDetailsModel> update(@PathVariable("id") Integer id, @RequestBody @Valid UserDetailsModel model) {
        return super.update(id, model);
    }

    @Override
    public ResponseEntity<UserDetailsModel> delete(@PathVariable("id") Integer id) {
        return super.delete(id);
    }

    @Override
    public ResponseEntity<List<UserDetailsModel>> search(@Valid @RequestBody SearchFilterModel searchModel) {
        return super.search(searchModel);
    }
}

