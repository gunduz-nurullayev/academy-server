package org.nazarov.software.server.spring.api;

import org.nazarov.software.server.hibernate.entity.Billing;
import org.nazarov.software.server.hibernate.service.BillingService;
import org.nazarov.software.server.hibernate.service.UserService;
import org.nazarov.software.server.hibernate.tool.DefaultDao;
import org.nazarov.software.server.spring.api.model.BillingModel;
import org.nazarov.software.server.spring.api.model.SearchFilterModel;
import org.nazarov.software.server.spring.util.ModelBuilder;
import org.nazarov.software.server.spring.util.ViewBuilderImpl;
import org.nazarov.software.server.tool.Server;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Shahin on 3/20/2017.
 */
@RestController
@RequestMapping(Server.API + Server.BILLING)
public class BillingRestController extends ViewBuilderImpl<BillingModel , Long> {

    public BillingRestController(@Autowired UserService userService , @Autowired BillingService billingService) {
        super(new BillingModel(userService), billingService);
    }

    @Override
    public ResponseEntity<List<BillingModel>> list() {
        return super.list();
    }

    @Override
    public ResponseEntity<BillingModel> get(@PathVariable("id") Long key) {
        return super.get(key);
    }

    @Override
    public ResponseEntity<BillingModel> update(@PathVariable("id") Long id,@RequestBody @Valid BillingModel model) {
        return super.update(id, model);
    }

    @Override
    public ResponseEntity<BillingModel> delete(@PathVariable("id") Long id) {
        return super.delete(id);
    }

    @Override
    public ResponseEntity<List<BillingModel>> search(@RequestBody @Valid SearchFilterModel searchModel) {
        return super.search(searchModel);
    }

    @Override
    public ResponseEntity<Void> create(@RequestBody @Valid BillingModel model, UriComponentsBuilder ucBuilder) {
        return super.create(new Billing() , model ,Server.BILLING,ucBuilder,model.getId());
    }
}
