package org.nazarov.software.server.spring.api.model;

import org.nazarov.software.server.hibernate.entity.Property;
import org.nazarov.software.server.spring.util.ModelBuilderImpl;

import javax.validation.constraints.NotNull;

/**
 * Created by Shahin on 1/4/2017.
 */
public class PropertyMapModel extends ModelBuilderImpl<PropertyMapModel , Property> {

    @NotNull
    private String key;
    @NotNull
    private String value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public PropertyMapModel convertToModel(Property entity) {
        PropertyMapModel model = new PropertyMapModel();
        model.setKey(entity.getKey());
        model.setValue(entity.getValue());
        return model;
    }

    @Override
    public Property convertToEntity(Property entity, PropertyMapModel model) {
        entity.setValue(model.getValue());
        return entity;
    }
}
