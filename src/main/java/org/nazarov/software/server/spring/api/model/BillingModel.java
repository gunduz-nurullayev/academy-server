package org.nazarov.software.server.spring.api.model;

import org.nazarov.software.server.hibernate.entity.Billing;
import org.nazarov.software.server.hibernate.entity.User;
import org.nazarov.software.server.hibernate.service.UserService;
import org.nazarov.software.server.hibernate.tool.enums.BillingType;
import org.nazarov.software.server.spring.util.ModelBuilderImpl;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * Created by Shahin on 3/20/2017.
 */
public class BillingModel extends ModelBuilderImpl<BillingModel , Billing>{

    private UserService userService;

    public BillingModel(UserService userService) {
        this.userService = userService;
    }

    public BillingModel() {
    }

    private long id;

    @NotNull
    private BillingType type;
    @NotNull
    private double amount = 0.0;
    @NotNull
    private long userId;
    @NotNull
    @Size(min = 20)
    private String details;

    private Date date;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BillingType getType() {
        return type;
    }

    public void setType(BillingType type) {
        this.type = type;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public BillingModel convertToModel(Billing entity) {
        BillingModel billingModel = new BillingModel();
        billingModel.setDetails(entity.getDetails());
        billingModel.setAmount(entity.getAmount());
        billingModel.setId(entity.getId());
        billingModel.setDate(entity.getDate());
        billingModel.setType(entity.getType());
        User user = entity.getUser();
        if(user != null)
            billingModel.setUserId(user.getId());
        return billingModel;
    }

    @Override
    public Billing convertToEntity(Billing entity, BillingModel model) {
        entity.setAmount(model.getAmount());
        entity.setType(model.getType());
        entity.setDate(model.getDate());
        entity.setDetails(model.getDetails());
        User user = userService.get(userId);
        if(user != null)
            entity.setUser(user);
        return entity;
    }
}
